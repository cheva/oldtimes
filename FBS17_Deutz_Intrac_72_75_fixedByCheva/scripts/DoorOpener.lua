--
-- DoorOpener
--
-- written by fruktor, visit: www.eifok-team.de
--

DoorOpener = {};

DoorOpener.modDir = g_currentModDirectory;

function DoorOpener.prerequisitesPresent(specializations)
    return true; 
end;

function DoorOpener:load(savegame)

    self.playerCallbackDoor = SpecializationUtil.callSpecializationsFunction("playerCallbackDoor"); 
    
    self.doorO = {};
    
    local trigger = Utils.indexToObject( self.components, getXMLString(self.xmlFile, string.format("vehicle.doorOpener#trigger")) );
    if trigger == nil then
        print("[Error::DoorOpener] node is OK, but trigger could not be located. Check line "..tostring(i+1).." in your vehicle.xml");
    end;
    
    local icIndex = getXMLInt(self.xmlFile, string.format("vehicle.doorOpener#icIndex", i));
    
    self.doorO.icIndex = icIndex;
    self.doorO.trigger = trigger;
    self.doorO.plIR = false;
        
    addTrigger( trigger, "playerCallbackDoor", self );                
    
end;

function DoorOpener:delete()
    if self.doorO.trigger ~= nil then
        removeTrigger(self.doorO.trigger);
    end;
end;

function DoorOpener:readStream(streamId, connection)
end;

function DoorOpener:writeStream(streamId, connection)
end;

function DoorOpener:mouseEvent(posX, posY, isDown, isUp, button)
end;

function DoorOpener:keyEvent(unicode, sym, modifier, isDown)
end;

function DoorOpener:update(dt)
    
    if self.doorO.plIR then
        g_currentMission:addHelpButtonText( g_i18n:getText("SET_DOOROPENER"), InputBinding.SET_DOOROPENER );    
        setTextColor(1.0, 1.0, 1.0, 1.0);
        setTextAlignment(RenderText.ALIGN_LEFT);                 
        renderText( 0.5, 0.06, 0.02, g_i18n:getText("SET_DOOROPENER") );
        if InputBinding.hasEvent(InputBinding.SET_DOOROPENER)then
            local obj = self.interactiveObjects[self.doorO.icIndex];
            if obj ~= nil then
                --self.setDoor(not obj.isOpen);
                self:doActionOnObject(self.doorO.icIndex);
            end;
            
            --self.vehicle:playAnimation(self.animation, dir, Utils.clamp(self.vehicle:getAnimationTime(self.animation), 0, 1), true);
        end
    end;
end;

function DoorOpener:onLeave()
end;

function DoorOpener:draw()
end;

function DoorOpener:playerCallbackDoor(triggerId, otherId, onEnter, onLeave, onStay)
    
    if onEnter and g_currentMission.controlPlayer and g_currentMission.player ~= nil and otherId == g_currentMission.player.rootNode then
        self.doorO.plIR = true;
    elseif onLeave and g_currentMission.player ~= nil and otherId == g_currentMission.player.rootNode then
        self.doorO.plIR = false;
    end;
    
end;


-- converted