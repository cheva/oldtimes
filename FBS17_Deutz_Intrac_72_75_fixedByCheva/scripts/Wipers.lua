--
-- Wipers
-- Specialization for Wipers
--
-- @author  	Manuel Leithner (SFM-Modding)
-- @version 	v1.0
-- @date  		24/10/10
-- @history:	v1.0 - Initial version
--
-- free for noncommerical-usage
--

Wipers = {};

function Wipers.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(AnimatedVehicle, specializations);
end;

function Wipers:load(savegame)
	self.wipers = {};
	self.wipers.anim = getXMLString(self.xmlFile, "vehicle.wiper#animName");
	self.wipers.speed = getXMLFloat(self.xmlFile, "vehicle.wiper#speed");
	self.wipers.isActive = false;
end;

function Wipers:delete()
end;

function Wipers:readStream(streamId, connection)
end;

function Wipers:writeStream(streamId, connection)
end;

function Wipers:mouseEvent(posX, posY, isDown, isUp, button)
end;

function Wipers:keyEvent(unicode, sym, modifier, isDown)
end;

function Wipers:update(dt)
end;

function Wipers:updateTick(dt)

	if self:getIsActive() then
		if self.wipers.isActive then
			if not self:getIsAnimationPlaying(self.wipers.anim) then
				self:playAnimation(self.wipers.anim, self.wipers.speed, 0, true);
			end;
		end;
		
		if g_currentMission.environment.lastRainScale > 0.1 and g_currentMission.environment.timeSinceLastRain < 30 then
			if not self.wipers.isActive then
				self:playAnimation(self.wipers.anim, self.wipers.speed, Utils.clamp(self:getAnimationTime(self.wipers.anim), 0, 1), true);
				self.wipers.isActive = true;
			end;
		else
			if self.wipers.isActive then
				self:setAnimationStopTime(self.wipers.anim, 1);
				self.wipers.isActive = false;
			end;
		end;
	end;
end;

function Wipers:draw()	
end;

function Wipers:onLeave()
	if self.wipers.isActive then
		self:stopAnimation(self.wipers.anim);	
	end;
end;

function Wipers:onEnter()
	if self.wipers.isActive then
		self:playAnimation(self.wipers.anim, self.wipers.speed, Utils.clamp(self:getAnimationTime(self.wipers.anim), 0, 1), true);	
	end;
end;