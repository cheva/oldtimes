--[[
AnimatedIndoorParts

Script to animate some indoor parts in combination with the gear box addon by mogli or drivecontrol by upsidedown

@Author:	Ifko[nator]
@Date:		11.11.2017

@Version:	1.5

@History:	1.0 @01.02.2017 - initial release in FS 17
			--------------------------------------------------------------------------------------------------
			1.5 @11.11.2017 - added functions for: oil pressure light, air pressure light and handbrake light
							- some code improvements
			--------------------------------------------------------------------------------------------------
]]

AnimatedIndoorParts = {};

function AnimatedIndoorParts.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(AnimatedVehicle, specializations); 
end;

function AnimatedIndoorParts:load(savegame)
	self.handThrottle = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.handThrottle#index"));
	self.clutch = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.clutch#index"));
	self.brake = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.brake#index"));
	self.throttle = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.throttle#index"));
	self.throttleLinkage = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.throttle#indexLinkage"));
	
	self.handThrottleMaxRotY = Utils.getNoNil(getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.handThrottle#maxRotY"), -55);
	self.brakeMaxRotX = Utils.getNoNil(getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.brake#maxRotX"), -35);
	self.throttleMaxRotX = Utils.getNoNil(getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.throttle#maxRotX"), -14);
	
	self.oldGear = 1;
	self.oldGear2 = self.oldGear;
	self.oldRange = 1;
	self.oldDirection = 1;
	
	self.oldNeutral = false;
	self.oldReverse = true;
	
	self.oilPressureLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.oilPressureLight#index"));
	self.airPressureLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.airPressureLight#index"));
	self.handbrakeLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.handbrakeLight#index"));
	
	self.oilPressureLightTurnOffTimer = 0;
	self.oilPressureLightDelay = Utils.getNoNil(getXMLString(self.xmlFile, "vehicle.animatedIndoorParts.oilPressureLight#delay"), 1) * 1000; --## default 1 sec
	self.motorWasOn = false;
	self.pedalWasToMetal = false;
	
	self.firstRunIsPending = true;
	self.stopToCount = false;
end;

function AnimatedIndoorParts:update(dt)
	local doInvertRotation = true;
	local isClutchActive = false;
	local gearboxAddonIsOn = self.mrGbMGetIsOn ~= nil and self:mrGbMGetIsOn();
	
	if self.clutch ~= nil then
		local rotX, _, _= getRotation(self.clutch);
		
		isClutchActive = rotX ~= 0;
	end;
	
	if self.driveControl ~= nil then
		if self.dCcheckModule ~= nil 
			and self:dCcheckModule("shuttle") ~= nil
			and self.driveControl.shuttle.isActive 
			and not gearboxAddonIsOn
		then
			doInvertRotation = self.driveControl.shuttle.direction > 0;
			
			if self.firstRunIsPending then
				if self.driveControl.shuttle.isActive and self.animations["groupGearShiftFromRange2ToR"] ~= nil then
					self:playAnimation("groupGearShiftFromRange2ToR", -1, nil, true);

					self.firstRunIsPending = false;
				end;
			end;
			
			if self.oldDirection ~= self.driveControl.shuttle.direction then
				if self.animations["setClutch"] ~= nil then
					self:playAnimation("setClutch", 1, nil, true);
				end;
				
				if self.oldReverse ~= not doInvertRotation then
					if self.animations["groupGearShiftFromRange2ToR"] ~= nil then
						if not doInvertRotation then
							self:playAnimation("groupGearShiftFromRange2ToR", 1, nil, true);
						else
							self:playAnimation("groupGearShiftFromRange2ToR", -1, nil, true);
						end;
					end;
					
					self.oldReverse = not doInvertRotation;
				end;
			
				self.oldDirection = self.driveControl.shuttle.direction;
			else
				local isShifting = self.animations["groupGearShiftFromRange2ToR"] ~= nil and self:getIsAnimationPlaying("groupGearShiftFromRange2ToR")
					or self.animations["setClutch"] and self:getIsAnimationPlaying("setClutch")
				;
			
				if self.animations["setClutch"] ~= nil and not isShifting then
					self:playAnimation("setClutch", -1, nil, true);
				end;
			end;
		end;
	end;
	
	if doInvertRotation then
		if self.axisForward <= 0 then
			if self.throttle ~= nil then
				local _, rotY, rotZ = getRotation(self.throttle);
				
				if not isClutchActive then	
					setRotation(self.throttle, math.rad(-self.throttleMaxRotX * self.axisForward), rotY, rotZ);
				else
					setRotation(self.throttle, 0, rotY, rotZ);
				end;
			
				if self.throttleLinkage ~= nil then
					local _, rotY, rotZ = getRotation(self.throttleLinkage);
					
					if not isClutchActive then	
						setRotation(self.throttleLinkage, math.rad(self.throttleMaxRotX * self.axisForward), rotY, rotZ);
					else
						setRotation(self.throttleLinkage, 0, rotY, rotZ);
					end;
				end;
			end;
		end;
		
		if self.axisForward >= 0 then
			if self.brake ~= nil then
				local _, rotY, rotZ = getRotation(self.brake);
				
				setRotation(self.brake, math.rad(self.brakeMaxRotX * self.axisForward), rotY, rotZ);
			end;
		end;
	else
		if self.axisForward >= 0 then
			if self.throttle ~= nil then
				local _, rotY, rotZ = getRotation(self.throttle);
			
				if not isClutchActive then	
					setRotation(self.throttle, math.rad(self.throttleMaxRotX * self.axisForward), rotY, rotZ);
				else
					setRotation(self.throttle, 0, rotY, rotZ);
				end;
			
				if self.throttleLinkage ~= nil then
					local _, rotY, rotZ = getRotation(self.throttleLinkage);
					
					if not isClutchActive then	
						setRotation(self.throttleLinkage, math.rad(-self.throttleMaxRotX * self.axisForward), rotY, rotZ);
					else
						setRotation(self.throttleLinkage, 0, rotY, rotZ);
					end;
				end;
			end;
		end;
		
		if self.axisForward <= 0 then
			if self.brake ~= nil then
				local _, rotY, rotZ = getRotation(self.brake);	
				
				setRotation(self.brake, math.rad(-self.brakeMaxRotX * self.axisForward), rotY, rotZ);
			end;
		end;
	end;

	if gearboxAddonIsOn then
		local currentGear = self:mrGbMGetCurrentGear();
		local neutralActive = self:mrGbMGetNeutralActive();
		local currentRange = self:mrGbMGetCurrentRange();
		local reverseActive = self:mrGbMGetReverseActive();
		local handThrottle = self:mrGbMGetHandThrottle();
		local forceChange = false;
		local direction = 1;
		
		if self.handbrakeLight ~= nil then
			setVisibility(self.handbrakeLight, neutralActive and self.isMotorStarted);	
		end;
		
		if currentRange == 3 then
			currentRange = self.oldRange;
			
			forceChange = true;
		end;
		
		if not self.isFirstRunDone then
			self:mrGbMSetNeutralActive(true);
		
			if self.animations["setHandbrake"] ~= nil then
				self:playAnimation("setHandbrake", 1, nil, true);
			end;
		
			self.isFirstRunDone = true;
		end;
		
		if self.handThrottle ~= nil then
			local rotX, _, rotZ = getRotation(self.handThrottle);
			
			setRotation(self.handThrottle, rotX, math.rad(self.handThrottleMaxRotY * handThrottle), rotZ);
		end;
		
		if (InputBinding.isPressed(InputBinding.gearboxMogliCLUTCH_3)
			or self.oldGear ~= currentGear
			or self.oldRange ~= currentRange
			or (self.oldNeutral ~= neutralActive and not self.isHired)
			or self.oldReverse ~= reverseActive
			)
		and not neutralActive then
			if self.animations["setClutch"] ~= nil then
				self:playAnimation("setClutch", 1, nil, true);
			end;
		else
			local isShifting = self.animations["gearShiftFrom" .. currentGear .. "To" .. self.oldGear2] ~= nil and self:getIsAnimationPlaying("gearShiftFrom" .. currentGear .. "To" .. self.oldGear2)
				or self.animations["gearShiftFrom" .. self.oldGear2 .. "To" .. currentGear] ~= nil and self:getIsAnimationPlaying("gearShiftFrom" .. self.oldGear2 .. "To" .. currentGear)
				or self.animations["gearShiftFrom" .. currentGear .. "ToN"] ~= nil and self:getIsAnimationPlaying("gearShiftFrom" .. currentGear .. "ToN")
				or self.animations["groupGearShiftFromRange" .. currentRange .. "ToN"] ~= nil and self:getIsAnimationPlaying("groupGearShiftFromRange" .. currentRange .. "ToN")
				or self.animations["groupGearShiftFromRange" .. currentRange .. "ToR"] ~= nil and self:getIsAnimationPlaying("groupGearShiftFromRange" .. currentRange .. "ToR")
				or self.animations["groupGearShiftFromRange1ToRange2"] ~= nil and self:getIsAnimationPlaying("groupGearShiftFromRange1ToRange2")
				or self.animations["groupGearShiftFromRangeRToN"] ~= nil and self:getIsAnimationPlaying("groupGearShiftFromRangeRToN")
				or self.animations["setClutch"] ~= nil and self:getIsAnimationPlaying("setClutch")
			;
		
			if self.animations["setClutch"] ~= nil and not isShifting then
				self:playAnimation("setClutch", -1, nil, true);
			end;
		end;
		
		if self.oldGear ~= currentGear then
			if not neutralActive then	
				if self.oldGear < currentGear then
					if self.animations["gearShiftFrom" .. self.oldGear .. "To" .. currentGear] ~= nil then
						self:playAnimation("gearShiftFrom" .. self.oldGear .. "To" .. currentGear, 1, nil, true);
					end;
		
				else	
					if self.animations["gearShiftFrom" .. currentGear .. "To" .. self.oldGear] ~= nil then
						self:playAnimation("gearShiftFrom" .. currentGear .. "To" .. self.oldGear, -1, nil, true);
					end;
				end;
			end;
			
			self.oldGear2 = self.oldGear;
			self.oldGear = currentGear;
		end;
		
		if self.oldRange ~= currentRange or forceChange then
			if not neutralActive then	
				if self.oldRange < currentRange then
					if self.animations["groupGearShiftFromRange" .. self.oldRange .. "ToRange" .. currentRange] ~= nil then
						self:playAnimation("groupGearShiftFromRange" .. self.oldRange .. "ToRange" .. currentRange, 1, nil, true);
					end;
				else
					if self.animations["groupGearShiftFromRange" .. currentRange .. "ToRange" .. self.oldRange] ~= nil then
						self:playAnimation("groupGearShiftFromRange" .. currentRange .. "ToRange" .. self.oldRange, -1, nil, true);
					end;
				end;
			end;
			
			self.oldRange = currentRange;
		end;
		
		if self.oldNeutral ~= neutralActive and not self.isHired then
			if reverseActive then
				direction = 1
			else
				direction = -1;
			end;
			
			if self.animations["groupGearShiftFromRange" .. currentRange .. "ToR"] ~= nil and not neutralActive then
				self:playAnimation("groupGearShiftFromRange".. currentRange .. "ToR", direction, nil, true);
			end;
			
			if neutralActive then
				direction = 1;
			else
				direction = -1;
			end;
			
			if self.animations["gearShiftFrom" .. currentGear .. "ToN"] ~= nil then
				self:playAnimation("gearShiftFrom" .. currentGear .. "ToN", direction, nil, true);
			end;
			
			if self.animations["groupGearShiftFromRange" .. currentRange .. "ToN"] ~= nil then
				self:playAnimation("groupGearShiftFromRange" .. currentRange .. "ToN", direction, nil, true);
			end;
			
			if reverseActive then
				if self.animations["groupGearShiftFromRangeRToN"] ~= nil then
					self:playAnimation("groupGearShiftFromRangeRToN", direction, nil, true);
				end;
			end;
			
			if self.animations["setHandbrake"] ~= nil then
				self:playAnimation("setHandbrake", direction, nil, true);
			end;
			
			self.oldNeutral = neutralActive;
		end;
		
		if self.oldReverse ~= reverseActive then
			if reverseActive then
				direction = 1
			else
				direction = -1;
			end;
			
			if self.animations["groupGearShiftFromRange" .. currentRange .. "ToR"] ~= nil and not neutralActive then
				self:playAnimation("groupGearShiftFromRange".. currentRange .. "ToR", direction, nil, true);
			end;
			
			self.oldReverse = reverseActive;
		end;
	else
		if self.handbrakeLight ~= nil then
			setVisibility(self.handbrakeLight, false);	
		end;
	end;
	
	if self.oilPressureLight ~= nil then
		if self.isMotorStarted then
			if not self.stopToCount then	
				if self.throttle ~= nil then
					local rotX, _, _ = getRotation(self.throttle);

					if rotX ~= 0 and not self.pedalWasToMetal then
						self.oilPressureLightTurnOffTimer = self.oilPressureLightDelay + g_currentMission.time;
						
						self.pedalWasToMetal = true;
					end;
				else
					self.oilPressureLightTurnOffTimer = self.oilPressureLightDelay + g_currentMission.time;
				end;
			end;
			
			if self.motorWasOn ~= self.isMotorStarted then
				setVisibility(self.oilPressureLight, true);

				self.motorWasOn = not self.isMotorStarted;
			end;
			
			if self.oilPressureLightTurnOffTimer ~= 0 then
				if self.oilPressureLightTurnOffTimer < g_currentMission.time then
					self.oilPressureLightTurnOffTimer = 0;
					setVisibility(self.oilPressureLight, false);
					self.stopToCount = true;
				else
					setVisibility(self.oilPressureLight, true);
				end;
			else
				if self.motorWasOn == self.isMotorStarted or self.stopToCount then	
					setVisibility(self.oilPressureLight, false);
				end;
			end;
		else			
			setVisibility(self.oilPressureLight, false);
			self.stopToCount = false;
			self.pedalWasToMetal = false;
		end;
	end;
	
	if self.airPressureLight ~= nil and self.brakeCompressor ~= nil then
		local currentAirPressurePercent = Utils.round(self.brakeCompressor.fillLevel / self.brakeCompressor.capacity, 2) * 100;
		
		if self.isEntered then	
			--setTextAlignment(RenderText.ALIGN_CENTER);
			--renderText(0.5, 0.5, 0.02, "currentAirPressurePercent = " .. currentAirPressurePercent);
		end;
		
		setVisibility(self.airPressureLight, currentAirPressurePercent < 41 and self.isMotorStarted);
	end;
end;

function AnimatedIndoorParts:delete()end;
function AnimatedIndoorParts:mouseEvent(posX, posY, isDown, isUp, button)end;
function AnimatedIndoorParts:keyEvent(unicode, sym, modifier, isDown)end;
function AnimatedIndoorParts:draw()end;