-- by modelleicher
-- www.schwabenmodding.de / www.ls-modcompany.de

-- adds additional load particle system 
-- particle system emits if the engine load is above a certain threshold (psThreshold value)
-- Version 1.1 -> using multiple exhaust particles / March 2018

newExhaustEffects = {};

function newExhaustEffects.prerequisitesPresent(specializations)
    return true;
end;

function newExhaustEffects:load(savegame)
    self.nrep = {}; -- all variables will be stored in this table to prevent interference with other scripts 
    self.nrep.maxMotorLoad = 0;
    self.nrep.speed = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.newExhaustEffects#speedSmoothingFactor"), 0.01);
	
    
    self.nrep.ps1 = {};
    ParticleUtil.loadParticleSystem(self.xmlFile, self.nrep.ps1, "vehicle.newExhaustEffects.particle1", self.components, false, nil, self.baseDirectory)
    self.nrep.ps2 = {};
    ParticleUtil.loadParticleSystem(self.xmlFile, self.nrep.ps2, "vehicle.newExhaustEffects.particle2", self.components, false, nil, self.baseDirectory)
    self.nrep.ps3 = {};
    ParticleUtil.loadParticleSystem(self.xmlFile, self.nrep.ps3, "vehicle.newExhaustEffects.particle3", self.components, false, nil, self.baseDirectory)
    self.nrep.ps4 = {};
    ParticleUtil.loadParticleSystem(self.xmlFile, self.nrep.ps4, "vehicle.newExhaustEffects.particle4", self.components, false, nil, self.baseDirectory)
	
	self.nrep.mLoadSmooth = 0;

	
end;


function newExhaustEffects:update(dt)    
    if self:getIsActive() and self.isMotorStarted then    
		-- get the current motor load
		self.nrep.maxMotorLoad = math.max(self.nrep.maxMotorLoad, self.motor.motorLoad);
        local mLoad =  self.motor.motorLoad / self.nrep.maxMotorLoad; -- converting the individual motor load to 0-1 range value     
		
		-- smooth the current motorload 
		if mLoad > self.nrep.mLoadSmooth then
			self.nrep.mLoadSmooth = math.min(self.nrep.mLoadSmooth + self.nrep.speed*dt, 1);
		elseif mLoad < self.nrep.mLoadSmooth then
			self.nrep.mLoadSmooth = math.max(self.nrep.mLoadSmooth - self.nrep.speed*1.3*dt, 0);
		end;
		
		-- turn on particles depending on motor load
		if self.nrep.mLoadSmooth > 0.34 and self.nrep.mLoadSmooth < 0.63 then 
			ParticleUtil.setEmittingState(self.nrep.ps1, true);
			ParticleUtil.setEmittingState(self.nrep.ps2, false)
			ParticleUtil.setEmittingState(self.nrep.ps3, false)
			ParticleUtil.setEmittingState(self.nrep.ps4, false)
		elseif self.nrep.mLoadSmooth > 0.63 and self.nrep.mLoadSmooth < 0.83 then
			ParticleUtil.setEmittingState(self.nrep.ps1, false);
			ParticleUtil.setEmittingState(self.nrep.ps2, true);
			ParticleUtil.setEmittingState(self.nrep.ps3, false);
			ParticleUtil.setEmittingState(self.nrep.ps4, false);
		elseif self.nrep.mLoadSmooth > 0.83 and self.nrep.mLoadSmooth < 0.94 then
			ParticleUtil.setEmittingState(self.nrep.ps1, false);
			ParticleUtil.setEmittingState(self.nrep.ps2, false);
			ParticleUtil.setEmittingState(self.nrep.ps3, true);
			ParticleUtil.setEmittingState(self.nrep.ps4, false);	
		elseif self.nrep.mLoadSmooth > 0.94 then
			ParticleUtil.setEmittingState(self.nrep.ps1, false);
			ParticleUtil.setEmittingState(self.nrep.ps2, false);
			ParticleUtil.setEmittingState(self.nrep.ps3, false);
			ParticleUtil.setEmittingState(self.nrep.ps4, true);	
		else
			ParticleUtil.setEmittingState(self.nrep.ps1, false);
			ParticleUtil.setEmittingState(self.nrep.ps2, false);
			ParticleUtil.setEmittingState(self.nrep.ps3, false);
			ParticleUtil.setEmittingState(self.nrep.ps4, false);	
		end;
			
    end;
end;
function newExhaustEffects:delete()
    if self.nrep.ps1 ~= nil then
        ParticleUtil.deleteParticleSystem(self.nrep.ps1);
		ParticleUtil.deleteParticleSystem(self.nrep.ps2);
		ParticleUtil.deleteParticleSystem(self.nrep.ps3);
		ParticleUtil.deleteParticleSystem(self.nrep.ps4); 
    end;
end;
function newExhaustEffects:onLeave()
    if self.nrep.ps1 then
		ParticleUtil.setEmittingState(self.nrep.ps1, false);
		ParticleUtil.setEmittingState(self.nrep.ps2, false);
		ParticleUtil.setEmittingState(self.nrep.ps3, false);
		ParticleUtil.setEmittingState(self.nrep.ps4, false);	
    end;
end;
function newExhaustEffects:mouseEvent(posX, posY, isDown, isUp, button)
end;
function newExhaustEffects:keyEvent(unicode, sym, modifier, isDown)
end;
function newExhaustEffects:draw()
end;
