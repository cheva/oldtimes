
cabinControls = {};

cabinControls.modDir = g_currentModDirectory;

function cabinControls.prerequisitesPresent(specializations)
    return true; 
end;

function cabinControls:load(savegame)
	-- Initialize our cabinControl object(?)(table?)
	self.CControls = {};
	-- Check joystick node
	self.CControls.joystick = {};
	self.CControls.joystick.node = Utils.indexToObject( self.components, getXMLString(self.xmlFile, "vehicle.cabinControls.joystick#node") );
	
	if self.CControls.joystick.node ~= nil then
		self.CControls.joystick.rotF = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.joystick#rotFront"), 0) );
		self.CControls.joystick.rotB = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.joystick#rotBack"), 0) );
		self.CControls.joystick.rotT = 0;
		self.CControls.joystick.rotC = 0;
		
		self.CControls.joystick.lastSpeedTarget = 999;
		self.CControls.joystick.limiterReached = true;
		self.CControls.joystick.direction = 1; -- DO NOT CHANGE THIS!
	end;
	
	
	-- Check shuttle node
	self.CControls.shuttle = {};
	self.CControls.shuttle.node = Utils.indexToObject( self.components, getXMLString(self.xmlFile, "vehicle.cabinControls.shuttle#node") );
	
	if self.CControls.shuttle.node ~= nil then
		--Forward position
		self.CControls.shuttle.rotFwd = {0, 0, 0};
		local rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, "vehicle.cabinControls.shuttle#rotFwd"));
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.CControls.shuttle.rotFwd = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
		--Backward position
		self.CControls.shuttle.rotBwd = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, "vehicle.cabinControls.shuttle#rotBwd"));
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.CControls.shuttle.rotBwd = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
		--Neutral/handbrake position
		self.CControls.shuttle.rotNeut = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, "vehicle.cabinControls.shuttle#rotNeut"));
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.CControls.shuttle.rotNeut = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
	end;
	
	-- Check handThrottle node
	self.CControls.handThrottle = {};
	self.CControls.handThrottle.node = Utils.indexToObject( self.components, getXMLString(self.xmlFile, "vehicle.cabinControls.handthrottle#node") );
	
	if self.CControls.handThrottle.node ~= nil then
		self.CControls.handThrottle.rotMinX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.handthrottle#rotMinX"), 0) );
		self.CControls.handThrottle.rotMaxX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.handthrottle#rotMaxX"), 0) );
	end;
	
	
	--Check brake node
	self.CControls.pedals = {};
	self.CControls.pedals.rotSpeed = 0.00125;
	self.CControls.brake = {};
	self.CControls.brake.node = Utils.indexToObject( self.components, getXMLString(self.xmlFile, "vehicle.cabinControls.brake#node") );
	
	if self.CControls.brake.node ~= nil then
		self.CControls.brake.rotMinX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.brake#rotMinX"), 0) );
		self.CControls.brake.rotMaxX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.brake#rotMaxX"), 0) );
	end;
	--Check clutch node
	self.CControls.clutch = {};
	self.CControls.clutch.node = Utils.indexToObject( self.components, getXMLString(self.xmlFile, "vehicle.cabinControls.clutch#node") );
	
	if self.CControls.clutch.node ~= nil then
		self.CControls.clutch.rotMinX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.clutch#rotMinX"), 0) );
		self.CControls.clutch.rotMaxX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.clutch#rotMaxX"), 0) );
	end;
	--Check throttle node
	self.CControls.throttle = {};
	self.CControls.throttle.node = Utils.indexToObject( self.components, getXMLString(self.xmlFile, "vehicle.cabinControls.throttle#node") );
	
	if self.CControls.throttle.node ~= nil then
		self.CControls.throttle.rotMinX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.throttle#rotMinX"), 0) );
		self.CControls.throttle.rotMaxX = math.rad( Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.cabinControls.throttle#rotMaxX"), 0) );
	end;
	
end;

function cabinControls:delete()

end;

function cabinControls:readStream(streamId, connection)
end;

function cabinControls:writeStream(streamId, connection)
end;

function cabinControls:mouseEvent(posX, posY, isDown, isUp, button)
end;

function cabinControls:keyEvent(unicode, sym, modifier, isDown)
end;

function cabinControls:update(dt)
	if self:getIsActive() then
		if self.isClient then
		
			--Joystick
			if self.CControls.joystick.node then
				cabinControls.updateJoystick(self, dt);
			end;
			
			--Shuttle
			local isOn = false;
			if self.mrGbMS ~= nil then
				isOn = self:mrGbMGetIsOnOff();
			end;
			if isOn then
				cabinControls.updateShuttleMrGbMS(self);
				cabinControls.updatePedalsMrGbMS(self, dt);
			elseif self.driveControl and self.driveControl.shuttle.isActive then
				cabinControls.updateShuttleDC(self);
				cabinControls.updatePedalsDC(self, dt);
			else
				cabinControls.updateShuttle(self);
				cabinControls.updatePedals(self, dt);
			end
			
			--HandThrottle
			if self.CControls.handThrottle.node and self.mrGbMS ~= nil then
				cabinControls.updateHandThrottle(self, dt);
			end;
			
			
			--Pedals
			--if self.CControls.brake.node and self.CControls.clutch.node and self.CControls.throttle.node then
				--cabinControls.updatePedals(self, dt);
			--end;
		end;
		end;
			
			
	
	if self.motor.speedLevel ~= 0 then
	end;
	
end;

function cabinControls:updateTick(dt)

end;

function cabinControls:onLeave()
end;

function cabinControls:draw()
end;

-- Joystick --
function cabinControls.updateJoystick(self, dt)
	local axisAccelerate = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
	local axisBrake = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
	local axisForward = 0 --Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	
	axisAccelerate = InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE);
	axisBrake = InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE)
	axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1);
	if InputBinding.isAxisZero(axisForward) then 
		axisAccelerate = InputBinding.getAnalogInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
		axisBrake = InputBinding.getAnalogInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
		axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	end
	
	local currSpeed = self.lastSpeed*3600;
	
	-- Speed limiter changed.
	if self.CControls.joystick.lastSpeedTarget ~= self.cruiseControl.speed and self.cruiseControl.state == Drivable.CRUISECONTROL_STATE_ACTIVE then
		self.CControls.joystick.limiterReached = false;
		self.CControls.joystick.lastSpeedTarget = self.cruiseControl.speed;
		if currSpeed < self.cruiseControl.speed then
			self.CControls.joystick.direction = -1;
		else
			self.CControls.joystick.direction = 1;
		end;
	end;
	
	if not self.CControls.joystick.limiterReached then
		axisAccelerate = self.CControls.joystick.direction;
		if self.CControls.joystick.direction == -1 and currSpeed > self.cruiseControl.speed then
			self.CControls.joystick.limiterReached = true;
		elseif self.CControls.joystick.direction == 1 and currSpeed < self.cruiseControl.speed then
			self.CControls.joystick.limiterReached = true;
		end;
	end;

	if axisAccelerate > 0 then
		self.CControls.joystick.rotT = -self.CControls.joystick.rotF*axisAccelerate;
	elseif axisBrake < 0 then
		self.CControls.joystick.rotT = self.CControls.joystick.rotB*axisBrake;
	else
		self.CControls.joystick.rotT = 0;
	end
	
	
	if self.CControls.joystick.rotC < self.CControls.joystick.rotT then
		self.CControls.joystick.rotC = self.CControls.joystick.rotC + dt/2000;
		if self.CControls.joystick.rotC > self.CControls.joystick.rotT then
			self.CControls.joystick.rotC = self.CControls.joystick.rotT;
		end;
	elseif self.CControls.joystick.rotC > self.CControls.joystick.rotT then
		self.CControls.joystick.rotC = self.CControls.joystick.rotC - dt/2000;
		if self.CControls.joystick.rotC < self.CControls.joystick.rotT then
			self.CControls.joystick.rotC = self.CControls.joystick.rotT;
		end;
	end;
	
	setRotation( self.CControls.joystick.node, self.CControls.joystick.rotC, 0, 0 );
end;

-- Shuttle --
-- GEARBOX -- 
function cabinControls.updateShuttleMrGbMS(self)
	if not self.CControls.shuttle.node then
		return;
	end;
	if self:mrGbMGetNeutralActive() then
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotNeut[1], self.CControls.shuttle.rotNeut[2], self.CControls.shuttle.rotNeut[3] );
	elseif self:mrGbMGetReverseActive() then
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotBwd[1], self.CControls.shuttle.rotBwd[2], self.CControls.shuttle.rotBwd[3] );
	else
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotFwd[1], self.CControls.shuttle.rotFwd[2], self.CControls.shuttle.rotFwd[3] );
	end;
end;
-- DRIVE CONTROL --
function cabinControls.updateShuttleDC(self)
	if not self.CControls.shuttle.node then
		return;
	end;
	--print(self.lastSpeed);
	if self.driveControl.shuttle.direction > 0 and self.lastSpeed > 0.0001 then
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotFwd[1], self.CControls.shuttle.rotFwd[2], self.CControls.shuttle.rotFwd[3] );
	elseif self.driveControl.shuttle.direction < 0 and self.lastSpeed > 0.0001 then
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotBwd[1], self.CControls.shuttle.rotBwd[2], self.CControls.shuttle.rotBwd[3] );
	else
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotNeut[1], self.CControls.shuttle.rotNeut[2], self.CControls.shuttle.rotNeut[3] );
	end;
end;
-- DEFAULT --
function cabinControls.updateShuttle(self)
	if not self.CControls.shuttle.node then
		return;
	end;
	--print(self.movingDirection );
	if self.movingDirection > 0.0001 then
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotFwd[1], self.CControls.shuttle.rotFwd[2], self.CControls.shuttle.rotFwd[3] );
	elseif self.movingDirection < -0.0001 then
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotBwd[1], self.CControls.shuttle.rotBwd[2], self.CControls.shuttle.rotBwd[3] );
	else
		setRotation( self.CControls.shuttle.node, self.CControls.shuttle.rotNeut[1], self.CControls.shuttle.rotNeut[2], self.CControls.shuttle.rotNeut[3] );
	end;
end;

-- HAND THROTTLE --
function cabinControls.updateHandThrottle(self)
	local throttle = self:mrGbMGetHandThrottle();
	local maxRot = self.CControls.handThrottle.rotMaxX - self.CControls.handThrottle.rotMinX;
	local currRotation = self.CControls.handThrottle.rotMinX + maxRot * throttle;
	--print("throttle: "..throttle);
	setRotation(self.CControls.handThrottle.node, currRotation, 0, 0 );
	

end;


-- Pedals --
-- GEARBOX -- 
function cabinControls.updatePedalsMrGbMS(self, dt)
	local axisAccelerate = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
	local axisBrake = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
	local axisForward = 0 --Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	
	axisAccelerate = InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE);
	axisBrake = InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE)
	axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1);
	if InputBinding.isAxisZero(axisForward) then 
		axisAccelerate = InputBinding.getAnalogInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
		axisBrake = InputBinding.getAnalogInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
		axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	end
	
	if axisForward < 0 then
		if not self:mrGbMGetNeutralActive() then
			axisAccelerate = axisBrake * -1;
		elseif self.driveControl.isActive then
			axisAccelerate = axisBrake * -1;
		end;
	elseif axisForward > 0 then
		if not self:mrGbMGetNeutralActive() then
			axisAccelerate = axisBrake * -1;
		elseif self.driveControl.isActive then
			axisAccelerate = axisBrake * -1;
		end;
	end
	
	--if self:mrGbMGetNeutralActive() then
		--clutchAxis = 1;
	--end;
	
	cabinControls.setPedals(self,axisAccelerate, axisBrake, 0, dt);
end;
-- DRIVE CONTROL --
function cabinControls.updatePedalsDC(self, dt)
	local axisAccelerate = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
	local axisBrake = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
	local axisForward = 0 --Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	
	axisAccelerate = InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE);
	axisBrake = InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE)
	axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1);
	if InputBinding.isAxisZero(axisForward) then 
		axisAccelerate = InputBinding.getAnalogInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
		axisBrake = InputBinding.getAnalogInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
		axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	end
	
	if self.driveControl.shuttle.direction > 0 and self.lastSpeed > 0.0001 then
		if axisForward < 0 then
			axisAccelerate = axisBrake * -1;
		end;
	elseif self.driveControl.shuttle.direction < 0 and self.lastSpeed > 0.0001 then
		if axisForward > 0 then
			axisAccelerate = axisBrake * -1;
		end;
	end;
	
	cabinControls.setPedals(self,axisAccelerate, axisBrake, 0, dt);
end;
-- DEFAULT --
function cabinControls.updatePedals(self, dt)
	local axisAccelerate = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
	local axisBrake = 0 --InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
	local axisForward = 0 --Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	
	axisAccelerate = InputBinding.getDigitalInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE);
	axisBrake = InputBinding.getDigitalInputAxis(InputBinding.AXIS_BRAKE_VEHICLE)
	axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1);
	if InputBinding.isAxisZero(axisForward) then 
		axisAccelerate = InputBinding.getAnalogInputAxis(InputBinding.AXIS_ACCELERATE_VEHICLE); 
		axisBrake = InputBinding.getAnalogInputAxis(InputBinding.AXIS_BRAKE_VEHICLE) 
		axisForward = Utils.clamp((axisAccelerate-axisBrake)*0.5, -1, 1); 
	end
	
	if self.movingDirection > 0.0001 then
		if axisForward < 0 then
			axisAccelerate = axisBrake * -1;
		else
			axisBrake = axisForward;
		end;
	elseif self.movingDirection < 0.0001 then
		if axisForward > 0 then
			axisAccelerate = axisBrake * -1;
		else
			axisBrake = axisForward;
	end;
	end
	cabinControls.setPedals(self, axisAccelerate, axisBrake, 0, dt);
end;
-- Set pedals --
function cabinControls.setPedals(self,axisAccelerate, axisBrake, clutchAxis, dt)
	local maxRot = 0;
	local currRotation = 0;
	local targetRot = 0;
	local cx, cy, cz;
	--brake
	if self.CControls.brake.node then
		maxRot = self.CControls.brake.rotMaxX - self.CControls.brake.rotMinX;
		targetRot = self.CControls.brake.rotMinX + maxRot * axisBrake;
		cx, cy, cz = getRotation(self.CControls.brake.node);
		local x;
		if axisBrake > 0 then
			x = cx + self.CControls.pedals.rotSpeed * dt;
			if x > targetRot then
				x = targetRot;
			end;
		else
			x = cx - self.CControls.pedals.rotSpeed * dt;
			if x < targetRot then
				x = targetRot;
			end;
		end;
			
		--if x < maxRot and x > self.CControls.brake.rotMinX then -- WARNING; 0 to -30 not 0 to 30 (inverted!)
			setRotation(self.CControls.brake.node, x, 0, 0 );
		--end;
		
	end;
	--clutch
	--if self.CControls.clutch.node then
		--maxRot = self.CControls.clutch.rotMaxX - self.CControls.clutch.rotMinX;
		--currRotation = self.CControls.clutch.rotMinX + maxRot * clutchAxis;
		--setRotation(self.CControls.clutch.node, currRotation, 0, 0 );
	--end;
	--throttle
	if self.CControls.throttle.node then
		maxRot = self.CControls.throttle.rotMaxX - self.CControls.throttle.rotMinX;
		targetRot = self.CControls.throttle.rotMinX + maxRot * axisAccelerate;
		cx, cy, cz = getRotation(self.CControls.throttle.node);
		local x;
		if axisAccelerate > 0 then
			x = cx + self.CControls.pedals.rotSpeed * dt;
			if x > targetRot then
				x = targetRot;
			end;
		else
			x = cx - self.CControls.pedals.rotSpeed * dt;
			if x < targetRot then
				x = targetRot;
			end;
		end;
			
		--if x < maxRot and x > self.CControls.throttle.rotMinX then 
			setRotation(self.CControls.throttle.node, x, 0, 0 );
		--end;
	
	
	
	
	
	
	
	
		--maxRot = self.CControls.throttle.rotMaxX - self.CControls.throttle.rotMinX;
		--currRotation = self.CControls.throttle.rotMinX + maxRot * axisForward;
		--setRotation(self.CControls.throttle.node, currRotation, 0, 0 );
	end;
end;

