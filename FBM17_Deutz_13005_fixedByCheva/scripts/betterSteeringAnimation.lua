-- by modelleicher
-- www.schwabenmodding.de
-- allows the drivers arm animation to use the steering knob to steer

betterSteeringAnimation = {};

function betterSteeringAnimation.prerequisitesPresent(specializations)
    return true;
end;

function betterSteeringAnimation:load(savegame)
    self.steerKnobRot = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.betterSteeringAnimation#steerKnobRot"));
end;

function betterSteeringAnimation:delete()
end;
function betterSteeringAnimation:mouseEvent(posX, posY, isDown, isUp, button)
end;
function betterSteeringAnimation:keyEvent(unicode, sym, modifier, isDown)
end;
function betterSteeringAnimation:update(dt)    
    if self:getIsActive() and self.steering ~= nil then
        local rx, ry, rz = getRotation(self.steering);
        setRotation(self.steerKnobRot, 0, ry*-1, 0);
    end; 
end;

function betterSteeringAnimation:draw()
end;
