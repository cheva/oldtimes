--[[

Interface: 1.4.4.0 1.4.4RC8

Author: GtX | Andy
Date: 16.06.2017
Version: 1.0

Support: http://ls-modcompany.com

Thankyou to:
Rival and Manuel Leithner for insperation from existing scripts.

History:
V 1.0 		@ 	16.06.2017 	- 	Project Started
V 1.0.1 	@ 	01.07.2017 	- 	Added Start Key animation option. 	-  GtX


----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	NOTES & How To Use: --
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

This script is designed to make your vehicle operate with an improved level of function.


1) Under the <onStart> </onStart> you may create multiple <startGroup        /> if you require more then one of a certain feature or wish to display another logo on a second screen longer.
Logo and main screen must be used together but logo display time can be different in each group. "warnLightCheck" allows you to display start up icons or logos.

2) <dayRunLights> <dayRunLights/> Allows for up to two different drl configs. This allows you to have one group shut off when lights are active and one group to stay on if required.

3) <onStartAnimations> <onStartAnimations/> Allows the use of animations to display fake gauges or even cabin part movments that can be set with a slower return modifier if required.
	Two types available (general use & Temp to allow for a differance if needed).

4) <realWarnLights> <realWarnLights/> Allows you to display certain features as a dash icon when activated or connected. It is recomended that these items are NOT set to visable in Giants Editor.

5) <gearDisplay> <gearDisplay/> Allows you to display a direction icon based on the status of the vehicle.

6) Brake Compressor fill level display. Both animations and digital are supported.

7) <mrGbHud> </mrGbHud> Allows you to display the current gear as taken from the Gearbox Addon by Mogli. Will also display a "R" when in reverse and a "H" when in Hold Mode.
If disabled (gearDisplay) will be used if indexed.

8)<insideActions> </insideActions>
	(8a) Allows you to animate your indicator switch and also the wiper switch (Wiper switch only when used with <cabAnimations><autoWipers>).
	(8b) A handbreak option is available and this will activate when vehicle is stopped. If used with GearboxAddon then it will activate when vehicle is on holdMode.
	(8c) A start key is available and this allows you to animate the engine starting key.


9) <cabAnimations> </cabAnimations> Allows you to have automatic wiper animations.

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	EXAMPLE FOR XML: --
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

<realCab>
	<onStart>
		<startGroup logoDisplay="0>15|0|7|0" mainScreenDisplay="0>15|0|7|1" extraScreenDisplay="13|3" />		
		<startGroup logoDisplay="0>15|0|7|5" logoDisplayTime="5000" mainScreenDisplay="0>15|0|7|6" />
		<startGroup warnLightCheck="0>15|0|7|0" warnLightDisplayTime="1500" />
	</onStart>
	
	<dayRunLights>
			<drlOne index="0>12|0" offWhenLightsActive="true" />
			<drlTwo index="0>9|5" />
	</dayRunLights>

	<onStartAnimations>
		<tempAnimation name="indoorTemp" tempCoolSpeed="0.4" />
		<onStartAnimation name="wheelMove" returnSpeed="0.8" />
	</onStartAnimations>
	
	<realWarnLights>
		<cruiseControl index="0>15|1|4|1|0" />
		<beaconLights index="0>15|1|4|0|0" flashDashIcon="true" />
		<hitchCanConnect index="0>12|1|0" />
		<hitchnoDetach index="0>12|1|1" />
		<wipersOn index="0>17|2|1" />
		<lowFuel index="0>17|2|0" lowFuelLevel="90" />
	</realWarnLights>
	
	<gearDisplay>
		<neutral index="0>15|0|7|5" />
		<reverse index="0>15|0|7|4" />
		<drive index="0>15|0|7|3" />
	</gearDisplay>
	
	<mrGbHud groupIndex="0>13|3|0|7|1"> 
		<gearNumbers numbers="0>13|3|0|7|1|0" precision="0"/>
		<iconIndex autoHold="0>13|3|0|7|1|1|1" reverse="0>13|3|0|7|1|1|0" />
	</mrGbHud>
		
	<insideActions>
		<indicators index="0>8|1|0|0" offPosition="0 0 0" leftTurn="0 15 0" rightTurn="0 -15 0" />
		<wipers index="0>8|1|1|0" offPosition="0 0 0" wipersOn="0 -25 0" />
		<handBreak index="0>14|1|0|0" offPosition="0 0 0" activated="-45 0 0" />
		<startKey index="0>12|0|10|0" engineOff="0 0 0" engineOn="0 0 65" />
	</insideActions>
	
	<cabAnimations>
		<autoWipers animationName="autoWipers" />
	</cabAnimations>
	
	<hudDisplay>
		<brakeCompressor numbers="0>13|3|0|6" precision="1"/>			
		<brakeCompressor animName="brakeComp"/>
	</hudDisplay>
</realCab>

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	END: --
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
]]

realCab ={};

function realCab.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(AnimatedVehicle, specializations)
		and	SpecializationUtil.hasSpecialization(Drivable, specializations);
end

function realCab:load(savegame)
	self.isLightsOn = realCab.isLightsOn
	
	self.realCab = {};
	
	local i = 0
	
    while true do
		local onStart = string.format("vehicle.realCab.onStart.startGroup(%d)", i)
		if not hasXMLProperty(self.xmlFile, onStart) then
            break
        end;
		
		local realInside = {}
		
		realInside.floatTime = 1;
		realInside.logoHoldTime = Utils.getNoNil(getXMLFloat(self.xmlFile, onStart.. "#logoDisplayTime"),3000);	--Load hang time of logo. Base defualt 3 Seconds.
		realInside.logoDisplay = Utils.indexToObject(self.components, getXMLString(self.xmlFile, onStart.."#logoDisplay"));	--Displayed before main screen.
		realInside.screenDisplay = Utils.indexToObject(self.components, getXMLString(self.xmlFile, onStart.."#mainScreenDisplay"));	--Displayed after logo.	
		realInside.hudDisplay = Utils.indexToObject(self.components, getXMLString(self.xmlFile, onStart.."#extraScreenDisplay"));	--Displayed with no wait time.
		realInside.warnHoldTime = Utils.getNoNil(getXMLFloat(self.xmlFile, onStart.. "#warnLightDisplayTime"),2000);	--Load hang time of warnLights. Base defualt 2 Seconds.
		realInside.warnLightCheck = Utils.indexToObject(self.components, getXMLString(self.xmlFile, onStart.."#warnLightCheck"));	--Display warn lights for set time on vehicle start.
	
		table.insert(self.realCab, realInside)
		i = i + 1
	end;
	
	self.dayRunningLights = {};
	local drl = "vehicle.realCab.dayRunLights";
	self.dayRunningLights.dayLightsOne = Utils.indexToObject(self.components, getXMLString(self.xmlFile, drl..".drlOne#index"));	--DRL One on engine active.
	self.dayRunningLights.turnOffWithLightsOne = Utils.getNoNil(getXMLBool(self.xmlFile, drl..".drlOne#offWhenLightsActive"), false);	--Show or hide DRL One when Lights are active. - Base defualt is set to off.
	self.dayRunningLights.dayLightsTwo = Utils.indexToObject(self.components, getXMLString(self.xmlFile, drl..".drlTwo#index"));	--DRL Two on engine active.
	self.dayRunningLights.turnOffWithLightsTwo = Utils.getNoNil(getXMLBool(self.xmlFile, drl..".drlTwo#offWhenLightsActive"), false);	--Show or hide DRL Two when Lights are active. - Base defualt is set to off.
	
	self.insideActive = {};
	local active = "vehicle.realCab.onStartAnimations";	
	self.insideActive.animationsOnStart = getXMLString(self.xmlFile, active.. ".onStartAnimation#name");	--Animation name for General onStart Animations.
	self.insideActive.tempAnimations = getXMLString(self.xmlFile, active.. ".tempAnimation#name");	--Animation name for fake Temp gauges
	self.insideActive.coolTime = Utils.getNoNil(getXMLFloat(self.xmlFile, active.. ".tempAnimation#tempCoolSpeed"),1);	--Return modifier for tempAnimationName.
	self.insideActive.aosTime = Utils.getNoNil(getXMLFloat(self.xmlFile, active.. ".onStartAnimation#returnSpeed"),1);	--Return modifier for onStartAnimationName.
	
	self.insideWarning = {};
	local warn = "vehicle.realCab.realWarnLights";	
	self.insideWarning.cruiseLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, warn..".cruiseControl#index"));	--Display a Cruise Control active light.
	self.insideWarning.noDetachLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, warn..".hitchNoDetach#index"));	--Display a flashing warning light if trailer can not be detached.
	self.insideWarning.canConnectLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, warn..".hitchCanConnect#index"));	--Display a ready to attach light when near an implament or trailer.
	self.insideWarning.beaconLights = Utils.indexToObject(self.components, getXMLString(self.xmlFile, warn..".beaconLights#index"));	--Display a static Beacon active light.	
	self.insideWarning.beaconIconFlash = Utils.getNoNil(getXMLBool(self.xmlFile, warn..".beaconLights#flashDashIcon"), false);	--Set if the Becon Active light is static or flashing.	
	self.insideWarning.wipersOnLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, warn..".wipersOn#index"));	--Displays a static Wipers active light if animated wipers are being used.
	self.insideWarning.lowFuelLight = Utils.indexToObject(self.components, getXMLString(self.xmlFile, warn..".lowFuel#index"));	--Displays a static Low Fuel light when fuel reaches set level.
	self.insideWarning.fuelLowLevel = Utils.getNoNil(getXMLFloat(self.xmlFile, warn.. ".lowFuel#lowFuelLevel"),80);	--Sets at what level the Low Fuel Light will turn on.
	
	
	self.insideActions = {};
	
	local action = "vehicle.realCab.insideActions";
	self.insideActions.turnIndex = Utils.indexToObject(self.components, getXMLString(self.xmlFile, action..".indicators#index") );	--Index of indicator stick.
	
	if self.insideActions.turnIndex ~= nil then
		self.insideActions.leftTurn = {0, 0, 0};
		local rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".indicators#leftTurn"));	--Position of indicator stick when turning left.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.leftTurn = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
		self.insideActions.rightTurn = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".indicators#rightTurn"));	--Position of indicator stick when turning right.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.rightTurn = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
		self.insideActions.turnOff = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".indicators#offPosition"));	--Position of indicator stick when off.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.turnOff = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
	end;
	
	self.insideActions.wiperIndex = Utils.indexToObject(self.components, getXMLString(self.xmlFile, action..".wipers#index") );		--Index of wiper switch. (This function can to be used with the AutoWiper function)
	
	if self.insideActions.wiperIndex ~= nil then
		self.insideActions.wipersOff = {0, 0, 0};
		local rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".wipers#offPosition"));	--Position of wiper switch when off.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.wipersOff = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
		self.insideActions.wipersOn = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".wipers#wipersOn"));		--Position of wiper switch when on.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.wipersOn = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
	end;
	
	self.insideActions.handBreakIndex = Utils.indexToObject(self.components, getXMLString(self.xmlFile, action..".handBreak#index") );		--Index of handBreak switch. Supports both standard and Gearbox Addon.
	
	if self.insideActions.handBreakIndex ~= nil then
		self.insideActions.handBreakOff = {0, 0, 0};
		local rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".handBreak#offPosition"));	--Position of handBreak switch when off.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.handBreakOff = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
		self.insideActions.handBreakOn = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".handBreak#activated"));		--Position of handBreak switch when on.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.handBreakOn = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
	end;
	
	self.insideActions.keyIndex = Utils.indexToObject(self.components, getXMLString(self.xmlFile, action..".startKey#index") );	--Index of indicator stick.
	
	if self.insideActions.keyIndex ~= nil then
		self.insideActions.engineOn = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".startKey#engineOn"));	--Position of key when engine is running.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.engineOn = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
		self.insideActions.engineOff = {0, 0, 0};
		rx, ry, rz = Utils.getVectorFromString(getXMLString(self.xmlFile, action..".startKey#engineOff"));	--Position of key when engine is turned off.
		if rx ~= nil and ry ~= nil and rz ~= nil then
			self.insideActions.engineOff = {math.rad(rx), math.rad(ry), math.rad(rz)};
		end;
	end;
	
	self.cabAnimations = {};
	local wipers = "vehicle.realCab.cabAnimations";
	self.cabAnimations.wipersActivated = false;
	self.cabAnimations.wipersAnimation = getXMLString(self.xmlFile, wipers.. ".autoWipers#animationName");	--Autowiper animation name.
	self.cabAnimations.wipersSpeed = Utils.getNoNil(getXMLFloat(self.xmlFile, wipers.. ".autoWipers#moveSpeed"),3);	--Autowiper speed of animations. (Defualt value is preset at 3 as it is a nice speed)
	
	
	self.insideGear = {};
	local autoGear = "vehicle.realCab.gearDisplay";
	self.insideGear.groupIndex = Utils.indexToObject (self.components, getXMLString(self.xmlFile, "vehicle.realCab.gearDisplay#groupIndex"));	--Main Group Index. Hides all displays if Gearbox Addon is active.
	
	if self.insideGear.reverse ~= nil and self.insideGear.groupIndex == nil then
		print("[Error::realCab.lua] reverse#index found but groupIndex is missing from vehicle.xml. Check vehicle.realCab.gearDisplay#groupIndex");
	end;
	if self.insideGear.drive ~= nil and self.insideGear.groupIndex == nil then
		print("[Error::realCab.lua] drive#index found but groupIndex is missing from vehicle.xml. Check vehicle.realCab.gearDisplay#groupIndex");
	end;
	if self.insideGear.neutral ~= nil and self.insideGear.groupIndex == nil then
		print("[Error::realCab.lua] neutral#index found but groupIndex is missing from vehicle.xml. Check vehicle.realCab.gearDisplay#groupIndex");
	end;
	
	self.insideGear.reverse = Utils.indexToObject(self.components, getXMLString(self.xmlFile, autoGear..".reverse#index"));	--Display Reverse Icon when moving back.
	self.insideGear.drive = Utils.indexToObject(self.components, getXMLString(self.xmlFile, autoGear..".drive#index"));	--Display Drive Icon when moving foward.
	self.insideGear.neutral = Utils.indexToObject(self.components, getXMLString(self.xmlFile, autoGear..".neutral#index"));	--Display Netural Icon when stopped.
	
	self.mrGb = {};
	
	local gearboxGroup = "vehicle.realCab.mrGbHud";	
	self.mrGb.groupIndex = Utils.indexToObject (self.components, getXMLString(self.xmlFile, "vehicle.realCab.mrGbHud#groupIndex"));	--Main Group Index. Hides all MRGB displays if Gearbox Addon is disabled.
	
	if self.mrGb.gearNumber ~= nil and self.mrGb.groupIndex == nil then
		print("[Error::realCab.lua] gearNumbers found but groupIndex missing from vehicle.xml. Check vehicle.realCab.mrGbHud#groupIndex");
	end;
	if self.mrGb.reverseIconIndex ~= nil and self.mrGb.groupIndex == nil then
		print("[Error::realCab.lua] iconIndex#reverse found but groupIndex missing from vehicle.xml. Check vehicle.realCab.mrGbHud#groupIndex");
	end;
	if self.mrGb.holdIconIndex ~= nil and self.mrGb.groupIndex == nil then
		print("[Error::realCab.lua] iconIndex#autoHold found but groupIndex missing from vehicle.xml. Check vehicle.realCab.mrGbHud#groupIndex");
	end;
	
	self.mrGb.gearNumber = VehicleHudUtils.loadHud(self, self.xmlFile, "gearNumbers", gearboxGroup);	--Adds a Digital ONLY display for MrGB Mod Gear Number using the standard indoorHud format.
	self.mrGb.reverseIconIndex = Utils.indexToObject (self.components, getXMLString(self.xmlFile, "vehicle.realCab.mrGbHud.iconIndex#reverse"));	--"Reverse" Icon Index (Displayed when Reverse is activated in MRGBM).
	self.mrGb.holdIconIndex = Utils.indexToObject (self.components, getXMLString(self.xmlFile, "vehicle.realCab.mrGbHud.iconIndex#autoHold"));	--"Netural" Icon Index (Displayed when Netural is activated in MRGBM).
	
	if self.mrGb.gearNumber ~= nil and self.mrGb.reverseIconIndex == nil then
		print("[Error::realCab.lua] gearNumbers found but iconIndex#reverse is missing from the vehicle.xml. Check vehicle.realCab.mrGbHud.iconIndex#reverse");
	end;
	if self.mrGb.gearNumber ~= nil and self.mrGb.holdIconIndex == nil then
		print("[Error::realCab.lua] gearNumbers found but iconIndex#autoHold is missing from the vehicle.xml. Check vehicle.realCab.mrGbHud.iconIndex#autoHold");
	end;
	
	self.generalHud = {};
	local generalGroup = "vehicle.realCab.hudDisplay";
	self.generalHud.bcAnimations = VehicleHudUtils.loadHud(self, self.xmlFile, "brakeCompressor", generalGroup);	--Adds a Analogue or Digital display for brakeCompressor fill volume using the standard indoorHud format.
	
end;
	
function realCab:update(dt)
	if self.isClient then
	
--**********************************************************************************************************	
-- realCab onStart
--**********************************************************************************************************
	
		for i=1, table.getn(self.realCab) do
			local realInside = self.realCab[i]
			
			if realInside.hudDisplay ~= nil then
				if self.isMotorStarted	
					then setVisibility(realInside.hudDisplay, true);
					else setVisibility(realInside.hudDisplay, false);
				end;
			end;
			
			if realInside.logoDisplay ~= nil and realInside.screenDisplay ~= nil then
				if self.isMotorStarted then 
					if realInside.floatTime < realInside.logoHoldTime 
						then setVisibility(realInside.logoDisplay, true); setVisibility(realInside.screenDisplay, false);
					end;
					if realInside.floatTime < (realInside.logoHoldTime + realInside.logoHoldTime) and realInside.floatTime > realInside.logoHoldTime
						then setVisibility(realInside.logoDisplay, false); setVisibility(realInside.screenDisplay, true);
					end;
					realInside.floatTime = realInside.floatTime + dt;
					else realInside.floatTime = 1; setVisibility(realInside.logoDisplay, false); setVisibility(realInside.screenDisplay, false);
				end;
			end;
			
			if realInside.warnLightCheck ~= nil then
				if self.isMotorStarted then 
					if realInside.floatTime < realInside.warnHoldTime 
						then setVisibility(realInside.warnLightCheck, true);
					end;
					if realInside.floatTime < (realInside.warnHoldTime + realInside.warnHoldTime) and realInside.floatTime > realInside.warnHoldTime
						then setVisibility(realInside.warnLightCheck, false);
					end;
					realInside.floatTime = realInside.floatTime + dt;
					else realInside.floatTime = 1; setVisibility(realInside.warnLightCheck, false);
				end;
			end;
		end;
		
--**********************************************************************************************************	
-- realCab dayLights
--**********************************************************************************************************
			
			if self.dayRunningLights.dayLightsOne ~= nil then
				local dayLightsOffOne = self.isMotorStarted and self:isLightsOn() and self.dayRunningLights.turnOffWithLightsOne
				if self.dayRunningLights.turnOffWithLightsOne == false then				
					else if self.isMotorStarted and dayLightsOffOne then
						setVisibility(self.dayRunningLights.dayLightsOne, false);
					else 
						setVisibility(self.dayRunningLights.dayLightsOne, true);
					end;
				end;
				if self.isMotorStarted and not dayLightsOffOne then
					setVisibility(self.dayRunningLights.dayLightsOne, true);
				else 
					setVisibility(self.dayRunningLights.dayLightsOne, false);
				end;
			end;
			if self.dayRunningLights.dayLightsTwo ~= nil then
				local dayLightsOffTwo = self.isMotorStarted and self:isLightsOn() and self.dayRunningLights.turnOffWithLightsTwo
				if self.dayRunningLights.turnOffWithLightsTwo == false then
					else if self.isMotorStarted and dayLightsOffTwo then
						setVisibility(self.dayRunningLights.dayLightsTwo, false);
					else 
						setVisibility(self.dayRunningLights.dayLightsTwo, true);
					end;
				end;
				if self.isMotorStarted and not dayLightsOffTwo then
					setVisibility(self.dayRunningLights.dayLightsTwo, true);
				else 
					setVisibility(self.dayRunningLights.dayLightsTwo, false);
				end;
			end;

--**********************************************************************************************************	
-- realCab gearDisplay
--**********************************************************************************************************
		if self:getIsActive() then
			local gearboxIsOn = false;
			if self.mrGbMS ~= nil then
				gearboxIsOn = self:mrGbMGetIsOnOff()
			end;

			if self.isMotorStarted and gearboxIsOn then
				if self.mrGb.groupIndex ~= nil then
					setVisibility(self.mrGb.groupIndex, true);
				end;
				if self.insideGear.groupIndex ~= nil then
					setVisibility(self.insideGear.groupIndex, false);
				end;				
				if self.mrGb.reverseIconIndex ~= nil then
					if self:mrGbMGetReverseActive() then
						setVisibility(self.mrGb.reverseIconIndex, true);
					else 
						setVisibility(self.mrGb.reverseIconIndex, false);
					end;
				end;
				if self.mrGb.holdIconIndex ~= nil then
					if self:mrGbMGetNeutralActive() then
						setVisibility(self.mrGb.holdIconIndex, true);
					else 
						setVisibility(self.mrGb.holdIconIndex, false);
					end;
				end;
				if self.insideGear.neutral ~= nil then
					setVisibility(self.insideGear.neutral, false);
				end;
				if self.insideGear.reverse ~= nil then
					setVisibility(self.insideGear.reverse, false);
				end;
				if self.insideGear.drive ~= nil then
					setVisibility(self.insideGear.drive, false);
				end;
				
			elseif self.isMotorStarted and not gearboxIsOn then
				if self.insideGear.groupIndex ~= nil then
					setVisibility(self.insideGear.groupIndex, true);
				end;
				if self.mrGb.groupIndex ~= nil then
					setVisibility(self.mrGb.groupIndex, false);
				end;
				if self.mrGb.reverseIconIndex ~= nil then
					setVisibility(self.mrGb.reverseIconIndex, false);
				end;
				if self.mrGb.holdIconIndex ~= nil then
					setVisibility(self.mrGb.holdIconIndex, false);
				end;
				if self:getLastSpeed() * self.speedDisplayScale < 1 and self.movingDirection == 0 then
					if self.insideGear.neutral ~= nil then
						setVisibility(self.insideGear.neutral, true);
					end;
				elseif self:getLastSpeed() * self.speedDisplayScale > 1 and self.movingDirection < 0 then
					if self.insideGear.reverse ~= nil then
						setVisibility(self.insideGear.reverse, true);
					end;
				elseif self:getLastSpeed() * self.speedDisplayScale > 1 and self.movingDirection > 0 then
					if self.insideGear.drive ~= nil then
						setVisibility(self.insideGear.drive, true);
					end;
				else
					if self.insideGear.neutral ~= nil then
						setVisibility(self.insideGear.neutral, false);
					end;
					if self.insideGear.reverse ~= nil then
						setVisibility(self.insideGear.reverse, false);
					end;
					if self.insideGear.drive ~= nil then
						setVisibility(self.insideGear.drive, false);
					end;
				end;
			end;
			
		end;

--**********************************************************************************************************	
-- realCab onStartAnimations
--**********************************************************************************************************
		
		if self.insideActive.animationsOnStart ~= nil then
			if self.isMotorStarted then
				self:playAnimation(self.insideActive.animationsOnStart, 1, self:getAnimationTime(self.insideActive.animationsOnStart), true);
			else
				self:playAnimation(self.insideActive.animationsOnStart, -self.insideActive.aosTime, self:getAnimationTime(self.insideActive.animationsOnStart), true);
			end;
		end;
			
		if self.insideActive.tempAnimations ~= nil then
			if self.isMotorStarted then
				self:playAnimation(self.insideActive.tempAnimations, 1, self:getAnimationTime(self.insideActive.tempAnimations), true);
			else
				self:playAnimation(self.insideActive.tempAnimations, -self.insideActive.coolTime, self:getAnimationTime(self.insideActive.tempAnimations), true);
			end;
		end;
		
--**********************************************************************************************************	
-- realCab warnLights
--**********************************************************************************************************
		
		if self.insideWarning.cruiseLight ~= nil then
			if self:getIsActive() then
				if self.cruiseControl.state == Drivable.CRUISECONTROL_STATE_OFF then
					setVisibility(self.insideWarning.cruiseLight, false);
				else
					if self.isMotorStarted then
						setVisibility(self.insideWarning.cruiseLight, true);
					end;
				end;
			end;
		end;
		
		if self.insideWarning.noDetachLight ~= nil and self.attacherJoints ~= nil then
		local warnFlash = Utils.clamp(math.cos( 9.0 * (g_currentMission.time/1000) ) + 0.2, 0, 1);
			if self.showDetachingNotAllowedTime > 0 and warnFlash == 0 then
				setVisibility(self.insideWarning.noDetachLight, true);
			else 
				setVisibility(self.insideWarning.noDetachLight, false);
			end;
		end;
		
		if self.insideWarning.canConnectLight ~= nil and self.attacherJoints ~= nil then
			if g_currentMission.attachableInMountRange == nil then
				setVisibility(self.insideWarning.canConnectLight, false);
			else 
				setVisibility(self.insideWarning.canConnectLight, true);
			end;
		end;
		
		if self.insideWarning.beaconLights ~= nil then
			local warnFlash = Utils.clamp(math.cos( 9.0 * (g_currentMission.time/1000) ) + 0.2, 0, 1);
			local flashIcon = self.insideWarning.beaconIconFlash;
			if self:getIsActive() then
				if self.beaconLightsActive and warnFlash == 0 and flashIcon then
					setVisibility(self.insideWarning.beaconLights, true);
				elseif self.beaconLightsActive and not flashIcon then
					setVisibility(self.insideWarning.beaconLights, true);
				else
					setVisibility(self.insideWarning.beaconLights, false);
				end;
			end;
		end;

		local wiperLightOn = false;
		if self.cabAnimations.wipersAnimation ~= nil then
			wiperLightOn = self.isMotorStarted and self.cabAnimations.wipersActivated == true
		end;
			
		if self.insideWarning.wipersOnLight ~= nil then
			if wiperLightOn then
				setVisibility(self.insideWarning.wipersOnLight, true);
			else
				setVisibility(self.insideWarning.wipersOnLight, false);
			end;
		end;
		
		if self.insideWarning.lowFuelLight ~= nil then
			if self.isMotorStarted then
				if self.fuelFillLevel <= self.insideWarning.fuelLowLevel then
					setVisibility(self.insideWarning.lowFuelLight, true);
				else
					setVisibility(self.insideWarning.lowFuelLight, false);
				end;
			elseif not self.isMotorStarted then
				setVisibility(self.insideWarning.lowFuelLight, false);
			end;
		end;
		
--**********************************************************************************************************	
-- realCab actionAnimations
--**********************************************************************************************************
		if self.insideActions.turnIndex ~= nil then	
			if self.turnLightState == Lights.TURNLIGHT_LEFT then
				setRotation(self.insideActions.turnIndex, self.insideActions.leftTurn[1], self.insideActions.leftTurn[2], self.insideActions.leftTurn[3] );
			elseif self.turnLightState == Lights.TURNLIGHT_RIGHT then
				setRotation(self.insideActions.turnIndex, self.insideActions.rightTurn[1], self.insideActions.rightTurn[2], self.insideActions.rightTurn[3] );
			else
				setRotation(self.insideActions.turnIndex, self.insideActions.turnOff[1], self.insideActions.turnOff[2], self.insideActions.turnOff[3] );
			end;
		end;
	
		local wipersOn = false;
		if self.cabAnimations.wipersAnimation ~= nil then
			wipersOn = self.isMotorStarted and self.cabAnimations.wipersActivated == true
		end;
	
		if self.insideActions.wiperIndex ~= nil then	
			if wipersOn then
				setRotation(self.insideActions.wiperIndex, self.insideActions.wipersOn[1], self.insideActions.wipersOn[2], self.insideActions.wipersOn[3] );
			else
				setRotation(self.insideActions.wiperIndex, self.insideActions.wipersOff[1], self.insideActions.wipersOff[2], self.insideActions.wipersOff[3] );
			end;
		end;
		
		local gearboxIsOn = false;
		if self.mrGbMS ~= nil then
			gearboxIsOn = self:mrGbMGetIsOnOff()
		end;
		
		if self.isMotorStarted and gearboxIsOn then
			if self.insideActions.handBreakIndex ~= nil then
				if self:mrGbMGetNeutralActive() then
					setRotation(self.insideActions.handBreakIndex, self.insideActions.handBreakOn[1], self.insideActions.handBreakOn[2], self.insideActions.handBreakOn[3] );
				else
					setRotation(self.insideActions.handBreakIndex, self.insideActions.handBreakOff[1], self.insideActions.handBreakOff[2], self.insideActions.handBreakOff[3] );
				end;
			end;
			
		elseif self.isMotorStarted and not gearboxIsOn then
			if self.insideActions.handBreakIndex ~= nil then
				if self.movingDirection == 0 then
					setRotation(self.insideActions.handBreakIndex, self.insideActions.handBreakOn[1], self.insideActions.handBreakOn[2], self.insideActions.handBreakOn[3] );
				else
					setRotation(self.insideActions.handBreakIndex, self.insideActions.handBreakOff[1], self.insideActions.handBreakOff[2], self.insideActions.handBreakOff[3] );
				end;
			end;
		
		elseif not self.isMotorStarted and self.movingDirection == 0 then
			if self.insideActions.handBreakIndex ~= nil then
				setRotation(self.insideActions.handBreakIndex, self.insideActions.handBreakOn[1], self.insideActions.handBreakOn[2], self.insideActions.handBreakOn[3] );
			end;
		end;
		
		if self.insideActions.keyIndex ~= nil then
			if self.isMotorStarted then
				setRotation(self.insideActions.keyIndex, self.insideActions.engineOn[1], self.insideActions.engineOn[2], self.insideActions.engineOn[3] );
			else
				setRotation(self.insideActions.keyIndex, self.insideActions.engineOff[1], self.insideActions.engineOff[2], self.insideActions.engineOff[3] );
			end;
		end;
	end;	
end;

function realCab:updateTick(dt)
	local gearboxIsOn = false;
	if self.mrGbMS ~= nil then
		gearboxIsOn = self:mrGbMGetIsOnOff()
	end;

	if self.isClient then
		if self:getIsMotorStarted() then
			if self.generalHud.bcAnimations ~= nil then
				VehicleHudUtils.setHudValue(self, self.generalHud.bcAnimations, self.brakeCompressor.fillLevel, self.brakeCompressor.capacity);
			end;
			if self.mrGb.gearNumber ~= nil and self.mrGb.groupIndex ~= nil and gearboxIsOn then
				VehicleHudUtils.setHudValue(self, self.mrGb.gearNumber, self:mrGbMGetGearNumber());
			end;
		end;
	end;
	
	if self.cabAnimations.wipersAnimation ~= nil then
		if self:getIsActive() and self.isMotorStarted then
			if self.cabAnimations.wipersActivated then
				if not self:getIsAnimationPlaying(self.cabAnimations.wipersAnimation) then
					self:playAnimation(self.cabAnimations.wipersAnimation, self.cabAnimations.wipersSpeed, 0, true);
				end;
			end;
			
			if g_currentMission.environment.lastRainScale > 0.1 and g_currentMission.environment.timeSinceLastRain < 30 then
				if not self.cabAnimations.wipersActivated then
					self:playAnimation(self.cabAnimations.wipersAnimation, self.cabAnimations.wipersSpeed, Utils.clamp(self:getAnimationTime(self.cabAnimations.wipersAnimation), 0, 1), true);
					self.cabAnimations.wipersActivated = true;
				end;
			else
				if self.cabAnimations.wipersActivated then
					self:setAnimationStopTime(self.cabAnimations.wipersAnimation, 1);
					self.cabAnimations.wipersActivated = false;
				end;
			end;
		end;
	end;
end;

function realCab:isLightsOn()
	local drlOn = true
	if self.lightsTypesMask == 0 then
		drlOn = false
	end;
	return drlOn
end;

function realCab:stopMotor()
	if self.generalHud.bcAnimations ~= nil then
		VehicleHudUtils.setHudValue(self, self.generalHud.bcAnimations, 0, self.brakeCompressor.capacity);
	end;
	if self.mrGb.GearNumber ~= nil then
		VehicleHudUtils.setHudValue(self, self.mrGb.GearNumber, 0, 0);
	end;
end;

--Unused Functions--

function realCab:readStream(streamId, connection)
end;

function realCab:writeStream(streamId, connection)
end;

function realCab:draw()
end;
	
function realCab:delete()
end;

function realCab:keyEvent(unicode, sym, modifier, isDown)
end;

function realCab:mouseEvent(posX, posY, isDown, isUp, button)
end;