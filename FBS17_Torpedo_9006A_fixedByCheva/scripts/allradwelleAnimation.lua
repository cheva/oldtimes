--
-- allradwelleAnimation
-- Specialization for Allrad
--
-- by modelleicher
-- www.schwabenmodding.bplaced.net


allradwelleAnimation = {};

function allradwelleAnimation.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(Motorized, specializations);
end;

function allradwelleAnimation:load(savegame)

	self.wellenCount = Utils.getNoNil(getXMLInt(self.xmlFile, "vehicle.Allradwellen#count"), 0);	 
	self.Wellen = {};
	self.Speed = {};
	if self.wellenCount ~= 0 and self.wellenCount ~= nil then
	    for i=1, self.wellenCount do
	        local Welle = string.format("vehicle.Allradwellen.Welle%d", i)
		    self.Wellen[i] = Utils.indexToObject(self.components, getXMLString(self.xmlFile, Welle .. "#index"));
	    end;
	end;
	
	-- Optionale Ausrichtung der Welle auf die Vorderachse, nur bei Pendelachsen erforderlich)
	self.welleDirection1 = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.Allradwellen.SetDirection#welle1"));
	self.welleDirection2 = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.Allradwellen.SetDirection#welle2"));
end;

function allradwelleAnimation:delete()
end;
function allradwelleAnimation:readStream(streamId, connection)   
end;
function allradwelleAnimation:writeStream(streamId, connection)  
end;
function allradwelleAnimation:mouseEvent(posX, posY, isDown, isUp, button)
end;
function allradwelleAnimation:keyEvent(unicode, sym, modifier, isDown)
end;
function allradwelleAnimation:update(dt)	
	if self:getIsActive() then	
	    if self.wellenCount ~= 0 and self.wellenCount ~= nil then
		  local getRx, getRy, getRz = getRotation(self.wheels[3].repr);			
	      for i=1, self.wellenCount do
	        setRotation(self.Wellen[i], 0, 0, getRx * 3);
	      end;
	    end;	
		
		-- Ausrichtung der Allradwelle auf die Vorderachse (Nur bei Pendelachsen erforderlich) --
		if self.welleDirection1 ~= nil and self.welleDirection2 ~= nil then
			local a2x, a2y, a2z = getWorldTranslation(self.welleDirection1);
			local b2x, b2y, b2z = getWorldTranslation(self.welleDirection2);
			x2, y2, z2 = worldDirectionToLocal(getParent(self.welleDirection1), b2x-a2x, b2y-a2y, b2z-a2z);
			setDirection(self.welleDirection1, x2, y2, z2, 0, 1, 0);			
			local a3x, a3y, a3z = getWorldTranslation(self.welleDirection2);
			local b3x, b3y, b3z = getWorldTranslation(self.welleDirection1);
			x3, y3, z3 = worldDirectionToLocal(getParent(self.welleDirection2), b3x-a3x, b3y-a3y, b3z-a3z);
			setDirection(self.welleDirection2, x3, y3, z3, 0, 1, 0);
		end;
		
    end; 
end;

function allradwelleAnimation:draw()
end;
