--
-- Author: Karol Gruszczyk(MySQly)
--
-- version ID   - 1.0
-- version date - 12/0/2017
-- 
--

ExhaustEffects = {}

function ExhaustEffects.prerequisitesPresent(specializations)
    return true
end

function ExhaustEffects:load(xmlFile)
    self.EF = {}
    self.EF.emittTime = 0
    
    self.EF.particleSystem = {}
    Utils.loadParticleSystem(self.xmlFile, self.EF.particleSystem, "vehicle.echaustParticleSystem", self.components, false, nil, self.baseDirectory)
    self.EF.psEmittTime = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.echaustParticleSystem#emittTime"), 1.0) * 1000
    self.EF.psEmittTime1 = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.echaustParticleSystem#emittTime1"), 1.0) * 1000
    self.EF.psEmittTime2 = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.echaustParticleSystem#emittTime2"), 1.0) * 1000
    self.EF.motorStartOffset = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.echaustParticleSystem#motorStartOffset"), 0.0) * 1000
    self.EF.startOffset1 = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.echaustParticleSystem#threshingStartOffset1"), 0.0) * 1000
    self.EF.startOffset2 = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.echaustParticleSystem#threshingStartOffset2"), 2.0) * 1000

end

function ExhaustEffects:delete()
    Utils.deleteParticleSystem(self.EF.particleSystem)
end

function ExhaustEffects:writeStream(streamId, connection)
end

function ExhaustEffects:readStream(streamId, connection)
end

function ExhaustEffects:mouseEvent(posX, posY, isDown, isUp, button)
end

function ExhaustEffects:keyEvent(unicode, sym, modifier, isDown)
end

function ExhaustEffects:update(dt)
end

function ExhaustEffects:updateTick(dt)
    if self:getIsActive() then
        if self.EF.threshingEmitting then
            self.EF.emittTime = self.EF.emittTime + dt
            if self.EF.emittTime > self.EF.startOffset2 + self.EF.psEmittTime2 then
                self.EF.threshingEmitting = false
            end

            local shouldEmitt1 = self.EF.emittTime > self.EF.startOffset1 and self.EF.emittTime < self.EF.startOffset1 + self.EF.psEmittTime1
            local shouldEmitt2 = self.EF.emittTime > self.EF.startOffset2 and self.EF.emittTime < self.EF.startOffset2 + self.EF.psEmittTime2
            Utils.setEmittingState(self.EF.particleSystem, shouldEmitt1 or shouldEmitt2)
        end
    end
    if self.isMotorStarted then
        if not self.EF.motorStartPlayed then
            self.EF.motorStartPlayed = true
            self.EF.motorEmitting = true
        end

        if self.EF.motorEmitting then
            self.EF.emittTime = self.EF.emittTime + dt
            if self.EF.emittTime > self.EF.motorStartOffset + self.EF.psEmittTime then
                self.EF.motorEmitting = false
            end

            local shouldEmitt = self.EF.emittTime > self.EF.motorStartOffset and self.EF.emittTime < self.EF.motorStartOffset + self.EF.psEmittTime
            Utils.setEmittingState(self.EF.particleSystem, shouldEmitt)
        end
    else
        self.EF.emittTime = 0
        self.EF.motorEmitting = false
        self.EF.motorStartPlayed = false
    end
end

function ExhaustEffects:onTurnedOn()
    if self:getIsActive() then
        self.EF.emittTime = 0
        self.EF.threshingEmitting = true
        self.EF.motorEmitting = false
    end
end

function ExhaustEffects:onTurnedOff()
    if self:getIsActive() then
        self.EF.threshingEmitting = false
    end
end

function ExhaustEffects:draw()
end