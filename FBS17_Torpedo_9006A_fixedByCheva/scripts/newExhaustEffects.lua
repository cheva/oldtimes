-- by modelleicher
-- www.schwabenmodding.bplaced.net

-- adds additional particle system 
-- particle system emits if the engine load is above a certain threshold (psThreshold value)
-- maxMotorLoad, the max. value motorLoad can reach witch the particular vehicle (use vehicle debug rendering to figure that value out)


newExhaustEffects = {};

function newExhaustEffects.prerequisitesPresent(specializations)
    return true;
end;

function newExhaustEffects:load(savegame)
	self.nrep = {}; -- all variables will be stored in this table to prevent interference with other scripts 
	self.nrep.maxMotorLoad = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.newExhaustEffects#maxMotorLoad"), 20);
	self.nrep.psThreshold = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.newExhaustEffects#psThreshold"), 0.85);
	
	self.nrep.ps = {};
	Utils.loadParticleSystem(self.xmlFile, self.nrep.ps, "vehicle.newExhaustEffects.particle", self.components, false, nil, self.baseDirectory)
end;


function newExhaustEffects:update(dt)	
	if self:getIsActive() and self.isMotorStarted then		
		local mLoad =  self.motor.motorLoad / self.nrep.maxMotorLoad; -- converting the individual motor load to 0-1 range value 	
		if mLoad > self.nrep.psThreshold then
			Utils.setEmittingState(self.nrep.ps, true);
		else
			Utils.setEmittingState(self.nrep.ps, false)
		end;
	end;
end;
function newExhaustEffects:delete()
	if self.nrep.ps ~= nil then
		Utils.deleteParticleSystem(self.nrep.ps);
	end;
end;
function newExhaustEffects:onLeave()
	if self.nrep.ps then
		Utils.setEmittingState(self.nrep.ps, false);
	end;
end;
function newExhaustEffects:mouseEvent(posX, posY, isDown, isUp, button)
end;
function newExhaustEffects:keyEvent(unicode, sym, modifier, isDown)
end;
function newExhaustEffects:draw()
end;
