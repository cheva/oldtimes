--
-- BuyableBlades
-- Specialization for BuyableBlades
--
-- @author  Stefan Maurus
-- @date  02/07/14

-- Copyright © Stefan Maurus, www.stefanmaurus.de

--[[
	<buyableBlades trigger="TRIGGER_INDEX"/>
]]

BuyableBlades = {};

function BuyableBlades.prerequisitesPresent(specializations)
	return true;
end;

function BuyableBlades:load(savegame)
	self.onBuyableBladesTrigger = BuyableBlades.onBuyableBladesTrigger;

	self.buyableBlades = {};
	self.buyableBlades.trigger = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.buyableBlades#trigger"));
	addTrigger(self.buyableBlades.trigger, "onBuyableBladesTrigger", self);
	self.buyableBlades.inRange = false;

	self.buyableBlades.inTrigger = {};
	self.buyableBlades.PlayerinTrigger = false;
	self.buyableBlades.nearestDistanceToView = 3; --m

    self.meshNode = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.buyableBlades#meshNode"));
    self.getMeshNodes = BuyableBlades.getMeshNodes;

    self.nodeId = self.components[1].node;

    g_currentMission:addNodeObject(self.nodeId, self)
end;

function BuyableBlades:getMeshNodes()
	return {self.meshNode};
end;

function BuyableBlades:delete()
	removeTrigger(self.buyableBlades.trigger);
end;

function BuyableBlades:readStream(streamId, connection)
end;

function BuyableBlades:writeStream(streamId, connection)
end;

function BuyableBlades:mouseEvent(posX, posY, isDown, isUp, button)
end;

function BuyableBlades:keyEvent(unicode, sym, modifier, isDown)
end;

function BuyableBlades:update(dt)
	for i=0, 15, 1 do
		if self.buyableBlades.inTrigger[i] ~= nil then
			if not self.buyableBlades.inTrigger[i].blades.allowsToChange and (self.buyableBlades.inTrigger[i].blades.currentUse/self.buyableBlades.inTrigger[i].blades.strength) <= 0.8 then
				self.buyableBlades.inTrigger[i].blades.allowsToChange = true;
			end;
			if self.isServer then
				if self.buyableBlades.inTrigger[i].blades.allowsToChange and self.buyableBlades.inTrigger[i].changeBlades and self:getFillLevel() > 0 then
					self.buyableBlades.inTrigger[i]:setCurrentUse(math.min(self.buyableBlades.inTrigger[i].blades.currentUse + self.buyableBlades.inTrigger[i].blades.strength*(math.min(self:getFillLevel(), self.buyableBlades.inTrigger[i].blades.blades)/self.buyableBlades.inTrigger[i].blades.blades), self.buyableBlades.inTrigger[i].blades.strength));
					self:setFillLevel(math.max(0, self:getFillLevel()-self.buyableBlades.inTrigger[i].blades.blades), FillUtil.fillTypeNameToInt["fertilizer"]);
					self.buyableBlades.inTrigger[i]:setChangeBlades(false);
					self.buyableBlades.inTrigger[i].blades.allowsToChange = false;
				end;
			end;
		end;
	end;

	if self.buyableBlades.PlayerinTrigger and g_currentMission.player ~= nil then
		g_currentMission:addExtraPrintText(string.format(g_i18n:getText("BLADES_LEFT"), self:getFillLevel()));
	end;

	if self:getFillLevel() <= 0 then
        if self.isServer then
            g_currentMission.vehiclesToDelete[self] = self;
        end;
    end;
end;

function BuyableBlades:updateTick(dt)
	if self.buyableBlades.trigger then
		if g_currentMission.player ~= nil then
			local px, py, pz = getWorldTranslation(self.buyableBlades.trigger);
			local vx, vy, vz = getWorldTranslation(g_currentMission.player.rootNode);
			local distance = Utils.vector3Length(px-vx, py-vy, pz-vz);
			if distance < self.buyableBlades.nearestDistanceToView then
				self.buyableBlades.PlayerinTrigger = true;
			else
				self.buyableBlades.PlayerinTrigger = false;
			end;
		end;
	else
		self.buyableBlades.PlayerinTrigger = false;
	end;
end;

function BuyableBlades:draw()
end;

function BuyableBlades:onAttach(attacherVehicle)
end;

function BuyableBlades:onDetach()
end

function BuyableBlades:onBuyableBladesTrigger(triggerId, otherId, onEnter, onLeave, onStay, otherShapeId)
	if onEnter or onLeave then
		local trailer = g_currentMission.objectToTrailer[otherShapeId];
		if trailer ~= nil then
			if trailer.changeBlades ~= nil then
				if onEnter then
					for i=0, 15, 1 do
						if self.buyableBlades.inTrigger[i] == nil then
							self.buyableBlades.inTrigger[i] = trailer;
							break;
						end;
					end;
				elseif onLeave then
					for i=0, 15, 1 do
						if trailer == self.buyableBlades.inTrigger[i] then
							self.buyableBlades.inTrigger[i] = nil;
							break;
						end;
					end;
				end;
			end;
		end;
	end;
end;
-- converted