--
-- ChangeableBlades
-- Specialization for ChangeableBlades
--
-- @author  Stefan Maurus
-- @date  02/07/14

-- Copyright © Stefan Maurus, www.stefanmaurus.de

--[[
	<changeableBlades referenceIndex="INDEX" blades="INT" demolitionFactor="FLOAT" strength="FLOAT" updateInterval="INT_MS"/>
]]

ChangeableBlades = {};

function ChangeableBlades.prerequisitesPresent(specializations)
	return SpecializationUtil.hasSpecialization(Pickup, specializations); -- for self.actLoad_IntSend in line 78 & 81
end;

function ChangeableBlades:load(savegame)
	self.setChangeBlades = SpecializationUtil.callSpecializationsFunction("setChangeBlades");
	self.setCurrentUse = SpecializationUtil.callSpecializationsFunction("setCurrentUse");

	self.blades = {};
	self.blades.isInRange = false;
	self.blades.blades = getXMLInt(self.xmlFile, "vehicle.changeableBlades#blades");
	self.blades.demolitionFactor = getXMLFloat(self.xmlFile, "vehicle.changeableBlades#demolitionFactor");
	self.blades.strength = getXMLFloat(self.xmlFile, "vehicle.changeableBlades#strength");
	self.blades.currentUse = self.blades.strength;
	self.blades.updateInterval = getXMLInt(self.xmlFile, "vehicle.changeableBlades#updateInterval");
	self.blades.timer = 0;
	self.blades.allowsToChange = false;
	self.changeBlades = false;

	self.hud3 = {};
    local uiScale = g_uiScale;
    if g_gameSettings ~= nil then
        uiScale = g_gameSettings:getValue("uiScale")
    end
	local width, height = getNormalizedScreenValues(225*uiScale, 20*uiScale);
	local w, h = getNormalizedScreenValues(0, 145*uiScale);
	self.hud3.xPos = 1-g_safeFrameOffsetX-width;
	self.hud3.yPos = h+g_safeFrameOffsetY;
	self.hud3.hudWidth = width;
	self.hud3.overlay_hud 	= Overlay:new("loadHUD_hud", 	Utils.getFilename("hud/hud_block.dds", self.baseDirectory), self.hud3.xPos, self.hud3.yPos, width, height);
	self.hud3.overlay_bar 	= Overlay:new("loadHUD_bar", 	Utils.getFilename("hud/hud_barY.dds", self.baseDirectory), self.hud3.xPos, self.hud3.yPos, width, height);

	self.fillScaleFix = self.forageWagon.fillScale;

	self.textBlink = {0.5, 1, 0.01, 0.5, false};
end;

function ChangeableBlades:delete()
end;

function ChangeableBlades:readStream(streamId, connection)
	self:setChangeBlades(streamReadBool(streamId), true);
	self:setCurrentUse(streamReadFloat32(streamId), true);
end;

function ChangeableBlades:writeStream(streamId, connection)
	streamWriteBool(streamId, self.changeBlades);
	streamWriteFloat32(streamId, self.blades.currentUse);
end;

function ChangeableBlades:mouseEvent(posX, posY, isDown, isUp, button)
end;

function ChangeableBlades:keyEvent(unicode, sym, modifier, isDown)
end;

function ChangeableBlades:setChangeBlades(bool, noEventSend)
	self.changeBlades = bool;
	SetChangeBladesEvent.sendEvent(self, bool, noEventSend);
end;

function ChangeableBlades:setCurrentUse(float, noEventSend)
	self.blades.currentUse = float;
	SetCurrentUseEvent.sendEvent(self, float, noEventSend);
end;

function ChangeableBlades:postLoad(savegame)
    if savegame ~= nil and not savegame.resetVehicles then
        self:setCurrentUse(Utils.getNoNil(getXMLFloat(savegame.xmlFile, savegame.key.."#currentBladeUse"),self.blades.strength));
    end;
end

function ChangeableBlades:getSaveAttributesAndNodes(nodeIdent)
	local attributes = 'currentBladeUse="'..self.blades.currentUse..'"';
	return attributes, nil;
end;

function ChangeableBlades:update(dt)
	if self:getIsActiveForInput() and self.blades.allowsToChange then
		if InputBinding.hasEvent(InputBinding.TOGGLE_AI) then
			self:setChangeBlades(true);
		end;
	end;

	if self.silageAdditives ~= nil then
		if self.silageAdditivesIsActiv and self:getUnitFillType(self.forageWagon.fillUnitIndex) == self.silageAdditives.usedForFillType then
			self.forageWagon.fillScale = (self.fillScaleFix+(self.fillScaleFix*self.silageAdditives.increase/100))*math.min(1,math.max(0.2,self.blades.currentUse/self.blades.strength*2));
		else
			self.forageWagon.fillScale = self.fillScaleFix*math.min(1,math.max(0.2,self.blades.currentUse/self.blades.strength*2));
		end;
	else
		self.forageWagon.fillScale = self.fillScaleFix*math.min(1,math.max(0.2,self.blades.currentUse/self.blades.strength*2));
	end;

	if self.blades.currentUse > 0 and self:allowPickingUp() and (self.forageWagon ~= nil and self.forageWagon.lastAreaBiggerZero) then
		self.blades.timer = self.blades.timer + dt;
		
		if self.blades.timer >= self.blades.updateInterval then
			self:setCurrentUse(math.max(self.blades.currentUse - self.blades.demolitionFactor, 0));
			self.blades.timer = 0;
		end;
	end;
end;

function ChangeableBlades:updateTick(dt)
end;

function ChangeableBlades:draw()
	setTextColor(1.0, 1.0, 1.0, 1.0);
	setTextBold(false);
	setTextAlignment(RenderText.ALIGN_LEFT);

	local demolition = (1-self.blades.currentUse/self.blades.strength)*100;
	local percentage = 1-self.blades.currentUse/self.blades.strength;

	setOverlayColor(self.hud3.overlay_bar.overlayId, 1, 0, 0, 1)
	self.hud3.overlay_bar.width = self.hud3.hudWidth * percentage;
	setOverlayUVs(self.hud3.overlay_bar.overlayId, 0, 0.05, 0, 1, percentage, 0.05, percentage, 1);

	self.hud3.overlay_bar:render();
	self.hud3.overlay_hud:render();

	setTextAlignment(RenderText.ALIGN_CENTER);
    local uiScale = g_uiScale;
    if g_gameSettings ~= nil then
        uiScale = g_gameSettings:getValue("uiScale")
    end
	local x, y = getNormalizedScreenValues(225*uiScale/2, 7);
	if self.blades.currentUse > 0 then
		renderText(self.hud3.xPos+x, self.hud3.yPos+y, 0.012*uiScale, g_i18n:getText("BLADES_CURRENTSTATUS").." "..string.format("%5.2f %%", demolition)); --/255)); --100*self.actLoad)); --
	else
		if not self.textBlink[5] then
			self.textBlink[1] = math.min(self.textBlink[1] + self.textBlink[3],self.textBlink[2]);
			if self.textBlink[1] == self.textBlink[2] then
				self.textBlink[5] = true;
			end;
		else
			self.textBlink[1] = math.max(self.textBlink[1] - self.textBlink[3],self.textBlink[4]);
			if self.textBlink[1] == self.textBlink[4] then
				self.textBlink[5] = false;
			end;
		end;
		renderText(self.hud3.xPos+x, self.hud3.yPos+(y/2), 0.018*self.textBlink[1], g_i18n:getText("BLADES_CHANGEBLADES"));
	end;
	setTextAlignment(RenderText.ALIGN_LEFT);

	if self.blades.allowsToChange then
		g_currentMission:addHelpButtonText(string.format(g_i18n:getText("BLADES_CHANGE")), InputBinding.TOGGLE_AI, nil, GS_PRIO_NORMAL)
	end;
end;

function ChangeableBlades:onAttach()
end;

function ChangeableBlades:onDetach()
end

--Event--

SetChangeBladesEvent = {};
SetChangeBladesEvent_mt = Class(SetChangeBladesEvent, Event);

InitEventClass(SetChangeBladesEvent, "SetChangeBladesEvent");

function SetChangeBladesEvent:emptyNew()
    local self = Event:new(SetChangeBladesEvent_mt);
    self.className="SetChangeBladesEvent";
    return self;
end;

function SetChangeBladesEvent:new(vehicle, changeBlades)
    local self = SetChangeBladesEvent:emptyNew()
    self.vehicle = vehicle;
	self.changeBlades = changeBlades;
    return self;
end;

function SetChangeBladesEvent:readStream(streamId, connection)
    local id = streamReadInt32(streamId);
    self.vehicle = networkGetObject(id);

	self.changeBlades = streamReadBool(streamId);
	if self.vehicle ~= nil then
		self.vehicle:setChangeBlades(self.changeBlades, true);
	end;
	if not connection:getIsServer() then
        g_server:broadcastEvent(SetChangeBladesEvent:new(self.vehicle, self.changeBlades), nil, connection, self.vehicle);
    end;
end;

function SetChangeBladesEvent:writeStream(streamId, connection)
    streamWriteInt32(streamId, networkGetObjectId(self.vehicle));
	streamWriteBool(streamId, self.changeBlades);
end;

function SetChangeBladesEvent.sendEvent(vehicle, changeBlades, noEventSend)
	if noEventSend == nil or noEventSend == false then
		if g_server ~= nil then
			g_server:broadcastEvent(SetChangeBladesEvent:new(vehicle, changeBlades), nil, nil, vehicle);
		else
			g_client:getServerConnection():sendEvent(SetChangeBladesEvent:new(vehicle, changeBlades));
		end;
	end;
end;

SetCurrentUseEvent = {};
SetCurrentUseEvent_mt = Class(SetCurrentUseEvent, Event);

InitEventClass(SetCurrentUseEvent, "SetCurrentUseEvent");

function SetCurrentUseEvent:emptyNew()
    local self = Event:new(SetCurrentUseEvent_mt);
    self.className="SetCurrentUseEvent";
    return self;
end;

function SetCurrentUseEvent:new(vehicle, currentUse)
    local self = SetCurrentUseEvent:emptyNew()
    self.vehicle = vehicle;
	self.currentUse = currentUse;
    return self;
end;

function SetCurrentUseEvent:readStream(streamId, connection)
    local id = streamReadInt32(streamId);
    self.vehicle = networkGetObject(id);

	self.currentUse = streamReadFloat32(streamId);
	if self.vehicle ~= nil then
		self.vehicle:setCurrentUse(self.currentUse, true);
	end;
	if not connection:getIsServer() then
        g_server:broadcastEvent(SetCurrentUseEvent:new(self.vehicle, self.currentUse), nil, connection, self.vehicle);
    end;
end;

function SetCurrentUseEvent:writeStream(streamId, connection)
    streamWriteInt32(streamId, networkGetObjectId(self.vehicle));
	streamWriteFloat32(streamId, self.currentUse);
end;

function SetCurrentUseEvent.sendEvent(vehicle, currentUse, noEventSend)
	if noEventSend == nil or noEventSend == false then
		if g_server ~= nil then
			g_server:broadcastEvent(SetCurrentUseEvent:new(vehicle, currentUse), nil, nil, vehicle);
		else
			g_client:getServerConnection():sendEvent(SetCurrentUseEvent:new(vehicle, currentUse));
		end;
	end;
end;
-- converted