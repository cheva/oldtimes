--
-- SilageAdditivesTank
-- Specialization for SilageAdditivesTank
--
-- @author  Stefan Maurus
-- @date  04/06/14

-- Copyright © Stefan Maurus, www.stefanmaurus.de

--[[
	<silageAdditivesTank litersPerSecond="FILL_LITERS_PER_SEC" trigger="TRIGGER_INDEX" emptyMass="KG"/>
]]

SilageAdditivesTank = {};

function SilageAdditivesTank.prerequisitesPresent(specializations)
	return true;
end;

function SilageAdditivesTank:load(savegame)
	self.onSilageAdditivesTankTrigger = SilageAdditivesTank.onSilageAdditivesTankTrigger;

	self.silageAdditivesTank = {};
	self.silageAdditivesTank.litersPerSecond = getXMLFloat(self.xmlFile, "vehicle.silageAdditivesTank#litersPerSecond");
	self.silageAdditivesTank.emptyMass = getXMLFloat(self.xmlFile, "vehicle.silageAdditivesTank#emptyMass");
	self.silageAdditivesTank.inRange = false;
	self.silageAdditivesTank.trigger = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.silageAdditivesTank#trigger"));
	addTrigger(self.silageAdditivesTank.trigger, "onSilageAdditivesTankTrigger", self);

	setMass(self.components[1].node, (self.silageAdditivesTank.emptyMass+self:getFillLevel())/1000);

	self.silageAdditivesTank.inTrigger = {};
	self.silageAdditivesTank.PlayerInTrigger = false;
	self.silageAdditivesTank.nearestDistanceToView = 3; --m

	self.timer = 0;
	self.interval = 1000; --ms

    self.meshNode = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.silageAdditivesTank#meshNode"));
    self.getMeshNodes = SilageAdditivesTank.getMeshNodes;

    self.nodeId = self.components[1].node;

    g_currentMission:addNodeObject(self.nodeId, self)
end;

function SilageAdditivesTank:getMeshNodes()
	return {self.meshNode};
end;

function SilageAdditivesTank:delete()
	removeTrigger(self.silageAdditivesTank.trigger);
end;

function SilageAdditivesTank:readStream(streamId, connection)
end;

function SilageAdditivesTank:writeStream(streamId, connection)
end;

function SilageAdditivesTank:mouseEvent(posX, posY, isDown, isUp, button)
end;

function SilageAdditivesTank:keyEvent(unicode, sym, modifier, isDown)
end;

function SilageAdditivesTank:update(dt)
	if self.silageAdditivesTank.inTrigger[0] ~= nil then
		for i=0, 15, 1 do
			if self.silageAdditivesTank.inTrigger[i] ~= nil then
				self.silageAdditivesTank.inTrigger[i].silageAdditivesTankAllowsFilling = self:getFillLevel() > 0;
				if self.isServer then
					if self.silageAdditivesTank.inTrigger[i].silageAdditivesTankIsFilling and self.silageAdditivesTank.inTrigger[i].silageAdditivesTankAllowsFilling then
						--self.timer=self.timer+dt;
						--if self.timer >= self.interval then
						--	self.timer = 0;
							local litersPerSecond = self.silageAdditivesTank.litersPerSecond;
							if self.silageAdditivesTank.litersPerSecond > self:getFillLevel() then
								litersPerSecond = self:getFillLevel();
							end;
							self.silageAdditivesTank.inTrigger[i]:setTankFillLevel(math.min(self.silageAdditivesTank.inTrigger[i].silageAdditivesFillLevel+litersPerSecond/1000*dt, self.silageAdditivesTank.inTrigger[i].silageAdditives.capacity));
							self:setFillLevel(math.max(0, self:getFillLevel()-self.silageAdditivesTank.litersPerSecond/1000*dt), FillUtil.fillTypeNameToInt["fertilizer"]);
						--end;
						setMass(self.components[1].node, (self.silageAdditivesTank.emptyMass+self:getFillLevel())/1000);
					end;
				end;
			end;
		end;
	end;

	if self.silageAdditivesTank.PlayerInTrigger and g_currentMission.player ~= nil then
		g_currentMission:addExtraPrintText(string.format(g_i18n:getText("SILAGEADD_FILLLEVEL"), self:getFillLevel()));
	end;

	if self:getFillLevel() == 0 then
        if self.isServer then
            g_currentMission.vehiclesToDelete[self] = self;
        end;
    end;
end;

function SilageAdditivesTank:updateTick(dt)
	if self.silageAdditivesTank.trigger then
		if g_currentMission.player ~= nil then
			local px, py, pz = getWorldTranslation(self.silageAdditivesTank.trigger);
			local vx, vy, vz = getWorldTranslation(g_currentMission.player.rootNode);
			local distance = Utils.vector3Length(px-vx, py-vy, pz-vz);
			if distance < self.silageAdditivesTank.nearestDistanceToView then
				self.silageAdditivesTank.PlayerInTrigger = true;
			else
				self.silageAdditivesTank.PlayerInTrigger = false;
			end;
		end;
	else
		self.silageAdditivesTank.PlayerInTrigger = false;
	end;
end;

function SilageAdditivesTank:draw()
end;

function SilageAdditivesTank:onAttach(attacherVehicle)
end;

function SilageAdditivesTank:onDetach()
end

function SilageAdditivesTank:onSilageAdditivesTankTrigger(triggerId, otherId, onEnter, onLeave, onStay, otherShapeId)
	if onEnter or onLeave then
		local trailer = g_currentMission.objectToTrailer[otherShapeId];
		if trailer ~= nil then
			if trailer.silageAdditivesFillLevel ~= nil then
				if onEnter then
					for i=0, 15, 1 do
						if self.silageAdditivesTank.inTrigger[i] == nil then
							self.silageAdditivesTank.inTrigger[i] = trailer;
							break;
						end;
					end;
				elseif onLeave then
					for i=0, 15, 1 do
						if trailer == self.silageAdditivesTank.inTrigger[i] then
							self.silageAdditivesTank.inTrigger[i].silageAdditivesTankAllowsFilling = false;
							self.silageAdditivesTank.inTrigger[i] = nil;
							break;
						end;
					end;
				end;
			end;
		end;
	end;
end;
-- converted