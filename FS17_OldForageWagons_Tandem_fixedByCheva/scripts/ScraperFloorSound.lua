--
-- ScraperFloorSound
-- Specialization scraper floor sound if different volumes on loading and unloading
--
-- @author  Stefan Maurus
-- @date  2016-10-05

-- Copyright � Stefan Maurus, www.stefanmaurus.de


ScraperFloorSound = {};

function ScraperFloorSound.prerequisitesPresent(specializations)
    return true;
end;

function ScraperFloorSound:load(savegame)
    if self.isClient then
        self.scraperFloorSound = {};
        self.scraperFloorSound.linkNode = Utils.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.scraperFloorSound#linkNode"));
--        self.scraperFloorSound.volumeLoading = getXMLFloat(self.xmlFile, "vehicle.scraperFloorSound#volumeLoading");
        self.scraperFloorSound.volumeUnloading = getXMLFloat(self.xmlFile, "vehicle.scraperFloorSound#volumeUnloading");

        self.scraperFloorSound.sample = SoundUtil.loadSample(self.xmlFile, {}, "vehicle.scraperFloorSound", nil, self.baseDirectory, self.scraperFloorSound.linkNode);
    end;
end;

function ScraperFloorSound:delete()
    if self.isClient then
        SoundUtil.deleteSample(self.scraperFloorSound.sample);
    end;
end;

function ScraperFloorSound:mouseEvent(posX, posY, isDown, isUp, button)
end;

function ScraperFloorSound:keyEvent(unicode, sym, modifier, isDown)
end;

function ScraperFloorSound:update(dt)
    if self.isClient then
        if self.tipState ~= 0 and self.tipState ~= Trailer.TIPSTATE_CLOSING then
            self.scraperFloorSound.volume3D = self.scraperFloorSound.volumeUnloading;
            SoundUtil.play3DSample(self.scraperFloorSound.sample, 0, 0, nil);
        else
            SoundUtil.stop3DSample(self.scraperFloorSound.sample);
        end;
    end;
end;

function ScraperFloorSound:updateTick(dt)
 end;

function ScraperFloorSound:onAttach()
end;

function ScraperFloorSound:onDetach()
end;

function ScraperFloorSound:draw()
end;
